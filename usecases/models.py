from django.db import models
import os

class File(models.Model):
    img_file = models.FileField(blank=False,
                                null=False,
                                upload_to=os.path.join('usecases', 'medical_extractor'))