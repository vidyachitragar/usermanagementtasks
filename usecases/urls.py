from django.conf.urls import url
from .views import MedicalExtractorView, InvoiceExtractorView

urlpatterns = [
    url('medical_extraction/', MedicalExtractorView.as_view()),
    url('invoice_extraction/', InvoiceExtractorView.as_view()),
]