from django.apps import AppConfig
from ml_platform.settings import MEDIA_ROOT
import os

class UsecasesConfig(AppConfig):
    name = 'usecases'

    invoice_excel_output_path = os.path.join("usecases", "invoice")
