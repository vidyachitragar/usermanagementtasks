import re
from datetime import datetime

import dateutil.parser
import pandas as pd
import os
def handle_dates(date_string):
    yourdate = dateutil.parser.parse(date_string)

    oldformat = yourdate
    datetimeobject = datetime.strptime(str(oldformat), '%Y-%m-%d  %H:%M:%S')
    new_date = datetimeobject.strftime('%m-%d-%Y')
    return str(new_date)
def dob_date(some_text):
    sentences = some_text.split('\n')
    date_of_birth_dict = {}
    date_of_birth_dict['Date Of Birth'] = ''
    list_birth = ['birth','Birth','BIRTH','Born','BORN','born']
    for word in list_birth:
        for sentence in sentences:
            if word in sentence:
                birth_sent = sentence
                # end_location = 0
                # sentence_lower = birth_sent.lower()
                # if len(list_birth) > 0:
                #     dob_text = list_birth[0]
                #     start_location = sentence_lower.find(dob_text)
                #     end_location = start_location + len(dob_text)
                date_list2 = re.findall(
                '([0-3]?\d)(?:st|nd|rd|th)?[\s-]*?\s*(Jan(?:uary)?(?:aury)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?'
                '|May|June?|July?|Aug(?:ust)?|Sept?(?:ember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?(?:emeber)?)??\s*.?,?[\s-]?'
                '([1-2]\d{3})|\s*([\d]{1,2}\s*[-/.|]\s*[\d]{1,2}\s*[-/.|]\s*[\d]{2,4})|(Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?'
                '|Apr(?:il)?|May|June?|July?|Aug(?:ust)?|Sept?(?:ember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)?\s*.?[-\s/]([0-3]?\d)(?:st|nd|rd|th)?[-,\s/]\s*([1-2]\d{3})',
                birth_sent.lower(), re.IGNORECASE)
                if len(date_list2) > 0:
                    extracted_date = ''
                    for item in date_list2[0]:
                        if len(item) > 0:
                            extracted_date += item + ' '
                #print(extracted_date)

            # format the date string
                    date_of_birth_dict['Date Of Birth'] = handle_dates(extracted_date)
                    break
            else:
                pass

    return date_of_birth_dict
if __name__ == "__main__":
    pass
