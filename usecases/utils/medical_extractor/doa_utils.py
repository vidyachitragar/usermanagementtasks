import re
from datetime import datetime

import dateutil.parser
import pandas as pd
import os
def handle_dates(date_string):
    yourdate = dateutil.parser.parse(date_string)

    oldformat = yourdate
    datetimeobject = datetime.strptime(str(oldformat), '%Y-%m-%d  %H:%M:%S')
    new_date = datetimeobject.strftime('%m-%d-%Y')
    return str(new_date)
def doa_date(some_text):
    x = some_text.split('\n')
    date_of_admission_dict = {}
    date_of_admission_dict['Date Of Admission'] = ''
    list_admitted = ['admit','Admit','ADMIT','Admission','ADMISSION','admission','adm','Adm','ADM']
    for i in list_admitted:
        for j in x:
            if i in j:
                admit_sent = j
                # end_location = 0
                # sentence_lower = admit_sent.lower()
                # if len(list_admitted) > 0:
                #     doa_text = list_admitted[0]
                #     start_location = sentence_lower.find(doa_text)
                #     end_location = start_location + len(doa_text)
                date_list = re.findall(
                '([0-3]?\d)(?:st|nd|rd|th)?[\s-]*?\s*(Jan(?:uary)?(?:aury)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?'
                '|May|June?|July?|Aug(?:ust)?|Sept?(?:ember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?(?:emeber)?)??\s*.?,?[\s-]?'
                '([1-2]\d{3})|\s*([\d]{1,2}\s*[-/.|]\s*[\d]{1,2}\s*[-/.|]\s*[\d]{2,4})|(Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?'
                '|Apr(?:il)?|May|June?|July?|Aug(?:ust)?|Sept?(?:ember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)?\s*.?[-\s/]([0-3]?\d)(?:st|nd|rd|th)?[-,\s/]\s*([1-2]\d{3})',
                admit_sent.lower(), re.IGNORECASE)
                if len(list_admitted) > 0 and len(date_list) > 0:
                    extracted_date = ''
                    for item in date_list[0]:
                        if len(item) > 0:
                            extracted_date += item + ' '


            # format the date string
                    date_of_admission_dict['Date Of Admission'] = handle_dates(extracted_date)
                    break

            else:
                pass

    return date_of_admission_dict
# required_information = doa_date(input_text)
# print(required_information)
if __name__ == "__main__":
    pass


