import spacy
import dateparser
import re
import dateutil.parser
from datetime import datetime
# from doa import doa_date
from usecases.utils.medical_extractor.doa_utils import doa_date
from usecases.utils.medical_extractor.dod_utils import dod_date
from usecases.utils.medical_extractor.dob_utils import dob_date


#extracting name of the person
def person_data(input_text):
    nlp = spacy.load("en_core_web_sm")
    doc = nlp(input_text)

    def expand_person_entities(doc):
        person_dict = {'Doctors': [], 'Patient': []}
        for ent in doc.ents:
            if ent.label_ == "PERSON":
                if ent.start != 0:
                    prev_token = doc[ent.start - 1]

                    if prev_token.text in ("Dr ", "DR.", "Dr.", "Doctor", "Doctor.", "Dr. ", "DR. "):
                        if ent.text not in person_dict['Doctors']:
                            person_dict['Doctors'].append(ent.text)
                    elif prev_token.text in ("Ms", "Ms.", "Mr ", "Mr."):
                        if ent.text not in person_dict['Patient']:
                            person_dict['Patient'].append(ent.text)
                    elif dateparser.parse(ent.text) is None:
                        continue
        # Below for loop converting the lists like value to string
        for key, value in person_dict.items():
            person_dict[key] = ','.join(map(str, value))
        return person_dict

    data = expand_person_entities(doc)
    return data


# Extracting drugs, symptoms, diseases from input_text
def detection(input_text):
    input_text = input_text.lower()
    data_dictionary = {'Drugs': ['Abilify', 'Acetaminophen', 'Acetaminophen and Hydrocodone', 'Acetazolamide', 'Actos',
                                 'Acyclovir', 'Adderall', 'Adderall XR', 'Advair Diskus', 'Benadryl', 'Benazepril',
                                 'Benicar',
                                 'Bentyl', 'Benzonatate', 'Benztropine', 'Betamethasone', 'Bethanechol', 'Bevacizumab',
                                 'Biaxin',
                                 'Bicalutamide', 'Biktarvy', 'Biotin', 'Bisacodyl', 'Bisoprolol', 'Boniva', 'Botox',
                                 'Celexa',
                                 'Cephalexin', 'Cetirizine', 'Chlorthalidone', 'Cholecalciferol', 'Cialis',
                                 'Cilostazol', 'Crocine',
                                 'Clopidogrel', 'Clotrimazole', 'Codeine', 'Colace', 'Colchicine', 'Compazine',
                                 'Concerta',
                                 'Dicyclomine', 'Diflucan', 'Digoxin', 'Dilantin', 'Dilaudid', 'Diltiazem', 'Diovan',
                                 'Docusate Sodium',
                                 'Domperidone', 'Donepezil', 'Dopamine', 'Doxazosin', 'Epclusa', 'Epidiolex',
                                 'Epinephrine',
                                 'EpiPen', 'Eplerenone', 'Ergocalciferol', 'Etanercept', 'Etodolac', 'Etoposide',
                                 'Flagyl', 'Flecainide',
                                 'Flexeril', 'Flomax', 'Flonase', 'Florastor', 'Fludrocortisone', 'Fluocinonide',
                                 'Fluorouracil',
                                 'Fluoxetine', 'Fluphenazine', 'Fluticasone', 'Ginkgo', 'Ginkgo biloba', 'Ginseng',
                                 'Gleevec',
                                 'Glimepiride', 'Glipizide', 'Glucagon', 'Glucophage', 'Glucosamine', 'Glucotrol',
                                 'Glucovance',
                                 'Glumetza', 'Humulin N', 'Humulin R', 'Hydralazine', 'Hydrea', 'Hydrochlorothiazide',
                                 'Hydrochlorothiazide and irbesartan',
                                 'Hydrochlorothiazide and lisinopril', 'Hydrochlorothiazide and losartan',
                                 'Hydrochlorothiazide and triamterene',
                                 'Hydrochlorothiazide and valsartan', 'Hydrocodone', 'FLUROSEMIDE', 'TYLENOL - OTC'],
                       'Diseases': ['Abdominal aortic aneurysm', 'Acne', 'Acute cholecystitis',
                                    'Acute lymphoblastic leukaemia', 'acute Bronchitis',
                                    'venous malformation', 'Acute pancreatitis', 'allergies', 'Alzheimer',
                                    'Anal cancer', 'asthma', 'chronic renal',
                                    'DIABETES MELLITUS', 'jaundice', 'womb cancer', 'thyroid', 'Tuberculosis (TB)',
                                    'Type 1 diabetes',
                                    'Type 2 diabetes', 'tonsillitis', 'Thyroid cancer', 'Coronary artery disease (CAD)',
                                    'Stomach ulcer', 'Intracranial pathology'
                                    ],
                       'Symptoms': ['pain in the abdomen', 'internal bleeding', 'high temperature', 'nausea',
                                    'vomiting', 'sweating',
                                    'loss of appetite', 'pale skin', 'tiredness', 'breathlessness', 'diarrhoea',
                                    'sneezing', 'Dementia',
                                    'hallucinations', 'low mood', 'personality changes', 'blocked nose',
                                    'Ankle swelling', 'Cough', 'hypertension',
                                    'Headache', 'Sore throat', 'Dizzy', 'Polyuria', 'Polydipsia', 'Blurred vision',
                                    'hyperlipidemia',
                                    'Diaphoresis', 'Agitation', 'Tremor', 'Palpitations', 'Insomnia', 'Confusion',
                                    'Lethargy', 'Somnolence',
                                    'Amnesia', 'arthritic', 'Stupor', 'Seizures', 'allergies', 'weight gain',
                                    'dry skin and hair', 'muscle aches',
                                    'weight loss', 'night sweats', 'fever', 'loss of muscle bulk', 'earache',
                                    'indigestion', 'heartburn',
                                    ]}
    detected_data_dict = {}
    for k, v in data_dictionary.items():
        temp = []
        for vdata in v:
            vd = vdata.lower()
            if vd in input_text:
                temp.append(vdata)
            else:
                continue
        detected_data_dict[k] = ','.join(map(str, temp))
    
    return detected_data_dict


# Extracting height and weight from input_text
def height_and_weight(input_string):
    height_and_weight_dict = {'Height': '', 'Weight': ''}
    height_and_weight_dict['Weight'] = ','.join(
        map(str, re.findall('(\d+\s(?:kg|g|kilos|Kilos))', input_string, re.IGNORECASE)))
    heightpattern = re.compile(r"[0-9]+\s*'\s*[0-9]{1,2}\"")
    height_and_weight_dict['Height'] = ','.join(map(str, re.findall(heightpattern, input_string)))
    
    return height_and_weight_dict

# Extracting temperature from input_text
def temperature_regex(input_text):
    temperature_dict={'Temperature':[]}
    detected_temperature = re.findall('(\d+\.\d{1,2})\s(degrees |degree |°)([CcFf])', input_text, re.IGNORECASE)
    #print(detected_temperature)
    if len(detected_temperature)>0:
        detected_temperature = [" ".join(i) for i in detected_temperature]
    temperature_dict['Temperature']=detected_temperature
    #Below for loop converting the lists like value to string
    for key, value in temperature_dict.items():
        temperature_dict[key]=','.join(map(str, value))
    
    return temperature_dict

# Extracting gender from the input text
def gender(input_text):
    regex_ = re.findall('(?:male|female)', input_text, re.IGNORECASE)
    gender_dict = {'Gender': []}
    gender_dict['Gender'].extend(regex_)
    
    # Below for loop converting the lists like value to string
    for key, value in gender_dict.items():
        gender_dict[key]=','.join(map(str, value))
    return gender_dict

def handle_dates(date_string):
    yourdate = dateutil.parser.parse(date_string)

    oldformat = yourdate
    datetimeobject = datetime.strptime(str(oldformat), '%Y-%m-%d  %H:%M:%S')
    new_date = datetimeobject.strftime('%m-%d-%Y')
    return str(new_date)

def get_date_of_birth(input_text):

    sentences = input_text.split('\n')
    date_of_birth_dict = {}
    date_of_birth_dict['Date Of Birth'] = ''

    for sentence in sentences:
        end_location = 0
        sentence_lower = sentence.lower()
        dob_text_list = re.findall(
            'birthdate\s*[:|=|-]?\s*|dob\s*[:|=|-]?\s*|dateofbirth\s*[:|=|-]?\s*|d.o.b\s*[:|=|-]?\s*|date_of_birth\s*[:|=|-]?\s*|date-of-birth\s*[:|=|-]?\s*|birth date\s*[:|=|-]?\s*|date of birth\s*[:|=|-]?\s*|d o b\s*[:|=|-]?\s*|d . o . b\s*[:|=|-]?\s*|date - of - birth\s*[:|=|-]?\s*|birth_date\s*[:|=|-]?\s*|d-o-b\s*[:|=|-]?\s*|d_o_b\s*[:|=|-]?\s*',
            sentence_lower)
        
        if len(dob_text_list) > 0:
            dob_text = dob_text_list[0]
            start_location = sentence_lower.find(dob_text)
            end_location = start_location + len(dob_text)

        dob_date_list = re.findall(
            '([0-3]?\d)(?:st|nd|rd|th)?[\s-]*?\s*(Jan(?:uary)?(?:aury)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?'
                '|May|June?|July?|Aug(?:ust)?|Sept?(?:ember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?(?:emeber)?)??\s*.?,?[\s-]?'
                '([1-2]\d{3})|\s*([\d]{1,2}\s*[-/.|]\s*[\d]{1,2}\s*[-/.|]\s*[\d]{2,4})|(Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?'
                '|Apr(?:il)?|May|June?|July?|Aug(?:ust)?|Sept?(?:ember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)?\s*.?[-\s/]([0-3]?\d)(?:st|nd|rd|th)?[-,\s/]\s*([1-2]\d{3})',
               sentence_lower[end_location:], re.IGNORECASE)

        if len(dob_text_list) > 0 and len(dob_date_list) > 0:
            # extracted_date = [i for i in dob_date_list[0] if i != ''][0]

            dob_text = dob_text_list[0]
            extracted_date = ''
            for item in dob_date_list[0]:
                if len(item) > 0:
                    extracted_date += item + ' '
           

            # format the date string
            date_of_birth_dict['Date Of Birth'] = handle_dates(extracted_date)

        else:
            pass
    return date_of_birth_dict


def get_date_of_admission(input_text):
    sentences = input_text.split('\n')
    date_of_admission_dict = {}
    date_of_admission_dict['Date Of Admission'] = ''
    for sentence in sentences:
        end_location = 0
        sentence_lower = sentence.lower()

        doa_text_list = re.findall(
            'dateofadmission\s*[:|=|-]?\s*|Medical Admission Date\s*[:|=|-]?\s*|Adm. Date\s*[:|=|-]?\s*|admitted date\s*[:|=|-]?\s*|admitted on\s*[:|=|-]?\s*|date of admission\s*[:|=|-]?\s*|date-of-admission\s*[:|=|-]?\s*|d-o-a\s*[:|=|-]?\s*|d.o.a\s*[:|=|-]?\s*|date_of_admission\s*[:|=|-]?\s*|admissiondate\s*[:|=|-]?\s*|admission date\s*[:|=|-]?\s*|d . o . a\s*[:|=|-]?\s*|date - of - admission\s*[:|=|-]?\s*|doa\s*[:|=|-]?\s*|d o a\s*[:|=|-]?\s*|admission_date\s*[:|=|-]?\s*|d_o_a\s*[:|=|-]?\s*|admit_date\s*[:|=|-]?\s*|admitdate\s*[:|=|-]?\s*|admit date\s*[:|=|-]?\s*',
            sentence_lower)
        if len(doa_text_list) > 0:
            doa_text = doa_text_list[0]
            start_location = sentence_lower.find(doa_text)
            end_location = start_location + len(doa_text)

        doa_date_list = re.findall(
            '([0-3]?\d)(?:st|nd|rd|th)?[\s-]*?\s*(Jan(?:uary)?(?:aury)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?'
                '|May|June?|July?|Aug(?:ust)?|Sept?(?:ember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?(?:emeber)?)??\s*.?,?[\s-]?'
                '([1-2]\d{3})|\s*([\d]{1,2}\s*[-/.|]\s*[\d]{1,2}\s*[-/.|]\s*[\d]{2,4})|(Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?'
                '|Apr(?:il)?|May|June?|July?|Aug(?:ust)?|Sept?(?:ember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)?\s*.?[-\s/]([0-3]?\d)(?:st|nd|rd|th)?[-,\s/]\s*([1-2]\d{3})',
                sentence_lower[end_location:], re.IGNORECASE)

        #print("\n\n----", doa_text_list, doa_date_list)
        if len(doa_text_list) > 0 and len(doa_date_list) > 0:
            # extracted_date = [i for i in dob_date_list[0] if i != ''][0]
            extracted_date = ''
            for item in doa_date_list[0]:
                if len(item) > 0:
                    extracted_date += item + ' '
            
            # format the date string
            date_of_admission_dict['Date Of Admission'] = handle_dates(extracted_date)
            break
        else:
            pass
    return date_of_admission_dict



def get_date_of_discharge(input_text):
    sentences = input_text.split('\n')
    date_of_discharge_dict = {}
    date_of_discharge_dict['Date Of Discharge'] = ''
    for sentence in sentences:
        end_location = 0
        sentence_lower = sentence.lower()
        dod_text_list = re.findall(
            'dateofdischarge\s*[:|=|-]?\s*|Discharge Dt\s*[:|=|-]?\s*|Medical Discharge Date\s*[:|=|-]?\s*|Discharge\s*[:|=|-]?\s*|Discharged On\s*[:|=|-]?\s*|date of discharge\s*[:|=|-]?\s*|date-of-discharge\s*[:|=|-]?\s*|d-o-d\s*[:|=|-]?\s*|d.o.d\s*[:|=|-]?\s*|date_of_discharge\s*[:|=|-]?\s*|dischargedate\s*[:|=|-]?\s*|discharge date\s*[:|=|-]?\s*|d . o . d\s*[:|=|-]?\s*|date - of - discharge\s*[:|=|-]?\s*|dod\s*[:|=|-]?\s*|d o d\s*[:|=|-]?\s*|discharge_date\s*[:|=|-]?\s*|d_o_d\s*[:|=|-]?\s*|discharge_date\s*[:|=|-]?\s*|dischargedate\s*[:|=|-]?\s*|discharge date\s*[:|=|-]?\s*',
            sentence_lower)
        if len(dod_text_list) > 0:
            dod_text = dod_text_list[0]
            start_location = sentence_lower.find(dod_text)
            end_location = start_location + len(dod_text)
        dod_date_list = re.findall(
            '([0-3]?\d)(?:st|nd|rd|th)?[\s-]*?\s*(Jan(?:uary)?(?:aury)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?'
                '|May|June?|July?|Aug(?:ust)?|Sept?(?:ember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?(?:emeber)?)??\s*.?,?[\s-]?'
                '([1-2]\d{3})|\s*([\d]{1,2}\s*[-/.|]\s*[\d]{1,2}\s*[-/.|]\s*[\d]{2,4})|(Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?'
                '|Apr(?:il)?|May|June?|July?|Aug(?:ust)?|Sept?(?:ember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)?\s*.?[-\s/]([0-3]?\d)(?:st|nd|rd|th)?[-,\s/]\s*([1-2]\d{3})',
                sentence_lower[end_location:], re.IGNORECASE)

        if len(dod_text_list) > 0 and len(dod_date_list) > 0:
            # extracted_date = [i for i in dob_date_list[0] if i != ''][0]
            extracted_date = ''
            for item in dod_date_list[0]:
                if len(item) > 0:
                    extracted_date += item + ' '
            #print(extracted_date)

            # format the date string
            date_of_discharge_dict['Date Of Discharge'] = handle_dates(extracted_date)
            break
        else:
            pass
    #print(date_of_discharge_dict)
    return date_of_discharge_dict


# Below is the main function that executes all the above functions
def required_data_detection(input_text):
    person = person_data(input_text)
    detected_disease_symptom_drug = detection(input_text)
    birth = get_date_of_birth(input_text)
    if len(birth['Date Of Birth']) == 0:
        birth = dob_date(input_text)
    else:
        pass
    admission = get_date_of_admission(input_text)
    if len(admission['Date Of Admission']) == 0:
        admission = doa_date(input_text)
    else:
        pass
    discharge = get_date_of_discharge(input_text)
    if len(discharge['Date Of Discharge'])==0:
        discharge = dod_date(input_text)
    else:
        pass
    ht_and_wt = height_and_weight(input_text)
    detected_temp = temperature_regex(input_text)
    gen = gender(input_text)
    detected_information = {**person,**birth,**admission,**discharge,**detected_temp, **detected_disease_symptom_drug, **ht_and_wt,
                            **gen}
    return detected_information
