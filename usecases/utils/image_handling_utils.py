import io
from PIL import Image

def convert_image_to_bytes(image_file_path):

    image = Image.open(image_file_path, 'r')
    image_byte_array = io.BytesIO()
    image.save(image_byte_array, format='PNG')
    image_byte_array = image_byte_array.getvalue()

    return image_byte_array