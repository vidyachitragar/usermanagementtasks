import re

import dateutil.parser
from datetime import datetime
import spacy

class InvoiceParser():
    def __init__(self,input_text):
        self.input_text = input_text


    def handle_dates(self,date_string):
        yourdate = dateutil.parser.parse(date_string)

        old_format = yourdate
        date_time_object = datetime.strptime(str(old_format), '%Y-%m-%d  %H:%M:%S')
        new_date = date_time_object.strftime('%m-%d-%Y')
        return str(new_date)

    def invoice_date(self,input_text):
        sentences = input_text.split('\n')
        date_of_invoice_dict = {}
        date_of_invoice_dict['InvoiceDate'] = ''

        for sentence in sentences:
            end_location = 0
            sentence_lower = sentence.lower()
            invoice_text_list = re.findall(
                'invoicedate\s*[:|=|-]?\s*|doi\s*[:|=|-]?\s*|dateofinvoice\s*[:|=|-]?\s*|d.o.i\s*[:|=|-]?\s*|date_of_invoice\s*[:|=|-]?\s*|date-of-invoice\s*[:|=|-]?\s*|invoice date\s*[:|=|-]?\s*|date of invoice\s*[:|=|-]?\s*|d o i\s*[:|=|-]?\s*|d . o . i\s*[:|=|-]?\s*|date - of - invoice\s*[:|=|-]?\s*|invoice_date\s*[:|=|-]?\s*|d-o-i\s*[:|=|-]?\s*|d_o_i\s*[:|=|-]?\s*',
                sentence_lower)
            print("invoice_text_list", invoice_text_list)
            if len(invoice_text_list) > 0:
                invoice_text = invoice_text_list[0]
                start_location = sentence_lower.find(invoice_text)
                end_location = start_location + len(invoice_text)
                invoice_date_list = re.findall(
                    '([0-3]?\d)(?:st|nd|rd|th)?[\s-]*?\s*(Jan(?:uary)?(?:aury)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?'
                    '|May|June?|July?|Aug(?:ust)?|Sept?(?:ember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?(?:emeber)?)??\s*.?,?[\s-]?'
                    '([1-2]\d{3})|\s*([\d]{1,4}\s*[-/.|]\s*[\d]{1,2}\s*[-/.|]\s*[\d]{2,4})|(Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?'
                    '|Apr(?:il)?|May|June?|July?|Aug(?:ust)?|Sept?(?:ember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)?\s*.?[-\s/]([0-3]?\d)(?:st|nd|rd|th)?\s*[-,\s/]\s*([1-2]\d{3})',
                    sentence_lower[end_location:], re.IGNORECASE)
                print(invoice_date_list)

            if len(invoice_text_list) > 0 and len(invoice_date_list) > 0:
                invoice_text = invoice_text_list[0]
                extracted_date = ''
                for item in invoice_date_list[0]:
                    if len(item) > 0:
                        extracted_date += item + ' '
                print(extracted_date)

                # format the date string
                date_of_invoice_dict['InvoiceDate'] = self.handle_dates(extracted_date)

            else:
                pass
        print(date_of_invoice_dict)
        return date_of_invoice_dict

    def final_gst(self,input_text):
        def gst_no(input_text):
            reg_gst = re.findall('(GSTIN|GST ID No|GSTIDNO|GST_ID_NO)\s*([:|#])\s*([a-zA-Z0-9]+)', input_text,
                                 re.IGNORECASE)
            return (reg_gst)

        out = gst_no(input_text)
        gst_dict = {'GSTIN': []}
        for i in out:
            gst_dict['GSTIN'].extend(i[2])
            break
        for key, value in gst_dict.items():
            gst_dict[key] = ''.join(map(str, value))
        print(gst_dict)
        return gst_dict

    def final_invoice(self,input_text):
        def invoice_number(input_string):
            reg_inv = re.findall('Invoice\s*(No|#|id|Number)[.]?\s*([#\s]|[:\s])\s*?([a-zA-z0-9]+[-]?([a-zA-z0-9]+)?)',
                input_text, re.IGNORECASE)
            return reg_inv

        out = invoice_number(input_text)
        invoice_dict = {'InvoiceNumber': []}
        for i in out:
            invoice_dict['InvoiceNumber'].extend(i[2])
            break

        for key, value in invoice_dict.items():
            invoice_dict[key] = ''.join(map(str, value))
        print(invoice_dict)
        return invoice_dict

    def final_amount(self,input_text):
        def total_amount(input_text):
            amount_list = re.findall(
                '(total|totalamount|total amount|Amount Due|totalinvoicevalue|total invoice value|total payable|invoice total|total net amount)\s*(:|-)?\s*(((Rs|rs|RS|\$|USD|E|KES|[P])?\s*(.)?\s*([0-9]+)([,.])?([0-9]+)?([.])?([0-9]+)?))',
                input_text.lower().replace('subtotal', ''), re.IGNORECASE)
            return amount_list

        out = total_amount(input_text)
        amount_dict = {'TotalAmount': []}
        for i in out:
            amount_dict['TotalAmount'].extend(i[3])
        for key, value in amount_dict.items():
            amount_dict[key] = ''.join(map(str, value))
        print(amount_dict)
        return amount_dict

    def company_name(self,input_text):
        data_list = ['Pvt. Ltd', 'OPC', 'LLP', 'Itd', 'LLC', 'Private Limited', 'Services Ltd', 'Ltd', 'Inc',
                     'Solutions', 'Productions']
        nlp = spacy.load('en_core_web_sm')
        doc = nlp(input_text)
        var = [(X.text, X.label_) for X in doc.ents]
        my_list = []
        for i in var:
            if i[1] == 'ORG':
                my_list.append(i[0])
        name_dict = {'Comapny Name': []}
        for i in data_list:
            for j in my_list:
                if i in j:
                    # print(j)
                    name_dict['Comapny Name'].extend(j)
                    break
        for key, value in name_dict.items():
            name_dict[key] = ''.join(map(str, value))
        return (name_dict)

    def extract_information(self):
        invoicenumber = self.final_invoice(self.input_text)
        companyname = self.company_name(self.input_text)
        gst = self.final_gst(self.input_text)
        invoicedate = self.invoice_date(self.input_text)
        totalamount = self.final_amount(self.input_text)
        detected_information = {**companyname, **invoicedate, **invoicenumber, **gst, **totalamount}
        # print(detected_information)
        return detected_information


if __name__ == "__main__":
    input_text = '''INVOICE LOGO East Repair Inc. 1912 Harvest Lane New York, NY 12210 BILL TO SHIP TO INVOICE # US-001 John Smith John Smith INVOICE DATE 11/02/2019 2 Court Square 3787 Pineview Drive New York, NY 12210 Cambridge, MA 12210 P.O.# 2312/2019 DUE DATE 26/02/2019 QTY DESCRIPTION UNIT PRICE AMOUNT Front and rear brake cables 100.00 100.00 2 New set of pedal arms 15.00 30.00 Labor 3hrs 5.00 15.00 Subtotal 145.00 Sales Tax 6.25% 9.06 TOTAL $154.06 TERMS & CONDITIONS Thank you Payment is due within 15 days Please make checks payable to: East Repair Inc.'''
    parser_obj = InvoiceParser(input_text)
    extracted_content = parser_obj.extract_information()
    print(extracted_content)
