from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import FileUploadParser
import pandas as pd
import os
import io
from PIL import Image
from .serializers import FileSerializer
from usecases.utils.medical_extractor.set_of_functions import required_data_detection
from usecases.utils.image_handling_utils import convert_image_to_bytes

from envisage.services.ocr_service import ImageToTextProcessor
from usecases.services.invoice_extraction_service import InvoiceParser
from .apps import UsecasesConfig
from commons.file_handling import ensure_dir

#Security
from rest_framework.permissions import IsAuthenticated
from analytics import models

class MedicalExtractorView(APIView):

    parser_class = (FileUploadParser,)

    #permission_classes = [IsAuthenticated]
    #serializer_class = serializers.AnalyticsDataBaseSerializer
    db = models.AnalyticsDataBase()


    def post(self, request):

        #serializer.save(user=request.user)
        print(self.__class__.__name__)
        self.db.create(self.__class__.__name__)
        #self.db.save(service_name=)
        #serial.save(=self.__class__.__name__)

        file_serializer = FileSerializer(data=request.data)
        if file_serializer.is_valid():
            file_serializer.save()

            img_file_path = file_serializer.data.get('img_file')
            print("-----------", img_file_path)
            img = Image.open(img_file_path, 'r')
            imgByteArr = io.BytesIO()
            img.save(imgByteArr, format='PNG')
            imgByteArr = imgByteArr.getvalue()
            extracted_text_obj = ImageToTextProcessor()
            [_, converted_text] = extracted_text_obj.azure_ocr(imgByteArr)

            print("converted text----", converted_text)
            result = required_data_detection(converted_text)
            print("extracted text is : ", result)
            # convert to excel
            df = pd.DataFrame.from_dict([result], orient='columns', dtype=None, columns=None)

            path_to_excel = 'records.xlsx'

            if os.path.isfile(path_to_excel):
                excel_df = pd.read_excel(path_to_excel)
                frames = [excel_df, df]
                df = pd.concat(frames)
            df.to_excel(path_to_excel, index=False)

            response_dict = {
                "data": {
                    "excel_path": path_to_excel
                },
                "message": "Success!"
            }
            return Response(response_dict, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class InvoiceExtractorView(APIView):

    parser_class = (FileUploadParser,)

    def post(self, request):

        file_serializer = FileSerializer(data=request.data)
        if file_serializer.is_valid():
            file_serializer.save()

            img_file_path = file_serializer.data.get('img_file')

            image_byte_array = convert_image_to_bytes(img_file_path)
            extracted_text_obj = ImageToTextProcessor()
            [_, converted_text] = extracted_text_obj.azure_ocr(image_byte_array)

            parser_obj = InvoiceParser(converted_text)
            extracted_content = parser_obj.extract_information()

            print(extracted_content)

            # convert to excel
            df = pd.DataFrame.from_dict([extracted_content], orient='columns', dtype=None, columns=None)
            # ensure_dir(UsecasesConfig.invoice_excel_output_path)
            # path_to_excel = os.path.join(UsecasesConfig.invoice_excel_output_path, 'records.xlsx')
            path_to_excel = 'records.xlsx'

            if os.path.isfile(path_to_excel):
                excel_df = pd.read_excel(path_to_excel)
                frames = [excel_df, df]
                df = pd.concat(frames)
            df.to_excel(path_to_excel, index=False)

            response_dict = {
                "data": {
                    "excel_path": path_to_excel
                },
                "message": "Success!"
            }
            return Response(response_dict, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
