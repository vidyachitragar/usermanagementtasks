from django.conf.urls import url
from .views import ToneDetectionView

urlpatterns = [
    url('tone_detection/', ToneDetectionView.as_view()),
]