from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, generics
from rest_framework.parsers import FileUploadParser
from .serializers import AudioFileSerializer
from voicebased.services import gender_detect_obj, female_tone_detect_obj, male_tone_detect_obj
import pandas as pd
import shutil
import librosa
from .utils.chunking_util import ChukingService
from .utils.visualization_graph import VizualizationService

import os


class ToneDetectionView(APIView):

    parser_class = (FileUploadParser,)

    def post(self, request):
        audio_file_serializer = AudioFileSerializer(data=request.data)
        if audio_file_serializer.is_valid():
            audio_file_serializer.save()

            audio_file_path = audio_file_serializer.data.get('audio_file')
            chunk_seconds = audio_file_serializer.data.get('chunk_seconds')

            audio_duration = librosa.get_duration(filename=audio_file_path)
            gender = gender_detect_obj.detect_tone(audio_file_path)
            # print(audio_file_path)

            # chunking logic
            chunk_obj = ChukingService()
            list_of_filenames = chunk_obj.chunking(audio_file_path, chunk_seconds)
            save_path = os.path.join('media', 'temp')
            tone_list = []
            chunk_duration_list = []
            for chunk_name in list_of_filenames:
                # print('#######################',filename)
                chunk_path = os.path.join(save_path, chunk_name)
                chunk_duration = librosa.get_duration(filename=chunk_path)

                if gender == 'female':
                    tone = female_tone_detect_obj.detect_tone(chunk_path)

                else:
                    tone = male_tone_detect_obj.detect_tone(chunk_path)

                tone_list.append(tone[0])
                chunk_duration_list.append(chunk_duration)
            # print('################### tone_list\n',tone_list)

            series = pd.Series(chunk_duration_list)
            chunk_cumsum = series.cumsum()

            viz_df = pd.DataFrame()
            viz_df['chunk_duration'] = chunk_cumsum
            viz_df['predicted_emotion'] = tone_list
            # print("\n\n###########viz_df\n",viz_df)

            modified_viz_df = viz_df.copy()
            modified_viz_df['predicted_emotion'] = modified_viz_df['predicted_emotion'].map(
                {'anger': 1, 'disgust': 2, 'fear': 3, 'sad': 4, 'neutral': 5, 'surprised': 6, 'happy': 7})
            # print("modified : \n", modified_viz_df)

            inflection_list = []
            inflection = 0
            pred_tones = list(modified_viz_df['predicted_emotion'])
            for idx, value in enumerate(pred_tones):
                # print("-------", idx)

                if idx == 0:
                    inflection = 0
                    inflection_list.append(inflection)

                if idx >= len(pred_tones) - 1:
                    break

                if pred_tones[idx + 1] - pred_tones[idx] == 0:
                    inflection = 0
                elif pred_tones[idx + 1] - pred_tones[idx] > 0:
                    inflection = 1
                else:
                    inflection = -1
                inflection_list.append(inflection)
            # print("inflection_list",inflection_list)
            viz_df["inflection"] = inflection_list
            # print(viz_df)

            # for checking inflection
            inflection_at_list = []
            for idx, inflection_val in enumerate(viz_df['inflection']):
                # print(idx, inflection_val)
                if idx == 0:
                    inflection_at = None
                    inflection_at_list.append(inflection_at)

                if idx >= len(viz_df['inflection']) - 1:
                    break

                if viz_df['inflection'][idx + 1] == viz_df['inflection'][idx]:
                    inflection_at = None
                    inflection_at_list.append(inflection_at)
                else:
                    chunk_val_current = viz_df.loc[idx, 'chunk_duration']
                    chunk_val_next = viz_df.loc[idx + 1, 'chunk_duration']
                    mid_pt = (chunk_val_current + chunk_val_next) / 2
                    inflection_at = mid_pt
                    inflection_at_list.append(inflection_at)

            # print("inflection_at_list :\n ",inflection_at_list)
            viz_df["inflection_at"] = inflection_at_list
            # print(viz_df)

            # viz_df['predicted_emotion'] = viz_df['predicted_emotion'].map(
            #     {1 : 'anger', 2 : 'disgust', 3 : 'fear', 4 : 'sad', 5 : 'neutral', 6 : 'surprised', 7 : 'happy'})
            # print("Final viz_df : \n", viz_df)

            viz_df = viz_df.where(viz_df.notnull(), None)
            # print(viz_df)

            chunk_list = []
            for i in range(len(viz_df)):
                chunk_dict = {
                    "time": viz_df['chunk_duration'][i],
                    "emotion": viz_df['predicted_emotion'][i],
                    "inflection": viz_df['inflection'][i],
                    "inflection_at": viz_df['inflection_at'][i]
                }
                chunk_list.append(chunk_dict)

            viz_obj = VizualizationService()
            viz_obj.create_viz(viz_df)
            viz_path = viz_obj.save_viz(audio_file_path)

            response_dict = {
                "data": {
                    "audio_duration": audio_duration,
                    "predicted_gender": gender[0],
                    "predicted_emotion": chunk_list,
                    "plot_path": viz_path
                },
                "message": "Success!"
            }
            viz_df.drop(columns=['chunk_duration', 'predicted_emotion'])
            modified_viz_df.drop(columns=['chunk_duration', 'predicted_emotion'])
            shutil.rmtree(save_path)  # deleting temp folder

            return Response(response_dict, status=status.HTTP_201_CREATED)
        else:
            response_dict = {
                "data": None,
                "message": audio_file_serializer.errors
            }
            return Response(response_dict, status=status.HTTP_400_BAD_REQUEST)