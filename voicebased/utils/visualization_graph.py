import matplotlib.pyplot as plt
import os
from sklearn import preprocessing

class VizualizationService:

    def __init__(self):
        self.fixed_tones = ['anger', 'disgust', 'fear', 'sad', 'neutral', 'surprised', 'happy']
        self.fixed_tone_numbers = [1, 2, 3, 4, 5, 6, 7]

    def create_viz(self, df):
        le = preprocessing.LabelEncoder()
        df['predicted_emotion'] = le.fit_transform(df['predicted_emotion'])
        df['predicted_emotion'] = df['predicted_emotion'] + 1

        plt.yticks(self.fixed_tone_numbers, self.fixed_tones)
        plt.ylim([0.5, 7.5])
        plt.plot(df['chunk_duration'], df['predicted_emotion'], 'bo')
        plt.plot(df['chunk_duration'], df['predicted_emotion'])
        plt.grid(axis='y', linestyle=':')
        # plt.show()
        return df

    def save_viz(self, audio_file_path):
        audio_filename = audio_file_path.split('/')[-1]
        audio_filename = audio_filename.split('.')[0]
        audio_filename = audio_filename + ".png"

        file_path = os.path.join("media", 'voicebased', 'tone_detection', 'visualization')
        if not os.path.exists(file_path):
            os.makedirs(file_path)

        path = os.path.join(file_path, audio_filename)
        plt.savefig(path)
        # close the graph object, else plots will be overwritten
        plt.close('all')
        return path