from pydub import AudioSegment
from pydub.utils import make_chunks
import os
from ml_platform.settings import MEDIA_ROOT

class ChukingService:

    def __init__(self):
        pass

    def chunking(self, audio_file_path, chunk_seconds):
        file_path = os.path.join(MEDIA_ROOT, 'temp')
        if not os.path.exists(file_path):
            os.makedirs(file_path)
            #print("Directory created in '{}' path".format(file_path))

        myaudio = AudioSegment.from_file(audio_file_path , "wav")
        chunk_length_ms = (chunk_seconds * 1000) # pydub calculates in millisec
        chunks = make_chunks(myaudio, chunk_length_ms) #Make chunks of one sec

        #Export all of the individual chunks as wav files
        chunk_filename = []
        for i, chunk in enumerate(chunks):
            chunk_name = "chunk{0}.wav".format(i)
            chunk_path = os.path.join(MEDIA_ROOT, 'temp', chunk_name)
            #print(chunk_path)
            chunk.export(chunk_path, format="wav")
            chunk_filename.append(chunk_name)
        return chunk_filename