import numpy as np
import sys
import warnings
import librosa
import librosa.display
# import os
# os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# ignore warnings
if not sys.warnoptions:
    warnings.simplefilter("ignore")

class ToneDetectionService:

    def __init__(self, label_obj, model_obj, mean_obj, std_obj):

        self.label = label_obj
        self.loaded_model = model_obj
        self.mean = mean_obj
        self.std = std_obj

    def detect_tone(self, PATH_TO_AUDIO):

        X, sample_rate = librosa.load(PATH_TO_AUDIO,
                                      res_type='kaiser_fast',
                                      duration=4.0,
                                      sr=44100,
                                      offset=0
                                      )

        sample_rate = np.array(sample_rate)
        # Random offset / Padding
        audio_duration = 4.0
        sampling_rate = 44100
        input_length = sampling_rate * audio_duration
        if len(X) > input_length:
            max_offset = len(X) - input_length
            offset = np.random.randint(max_offset)
            X = X[offset:(input_length+offset)]
        else:
            if input_length > len(X):
                max_offset = input_length - len(X)
                offset = np.random.randint(max_offset)
            else:
                offset = 0
            X = np.pad(X, (offset, int(input_length) - len(X) - offset), "constant")

        mfccs = librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=30)
        # print("\n\nMFCCS before expan dims", mfccs.shape, mfccs)
        mfccs = np.expand_dims(mfccs, axis=-1)
        # print("\n\nMFCCS after expan dims", mfccs.shape, mfccs)

        mfccs = (mfccs - self.mean)/self.std
        # print("\n\nMFCCS after normalization", mfccs)
        mfccs = np.expand_dims(mfccs, axis=0)

        newpred = self.loaded_model.predict(mfccs, batch_size=100, verbose=2)

        # Get the final predicted label
        final = newpred.argmax(axis=1)
        final = final.astype(int).flatten()
        final = self.label.inverse_transform(final)
        return final

if __name__ == "__main__":
    PATH_TO_MODEL = "Emotion_Model_MFCC_345.h5"
    PATH_TO_LABELS = 'labels'
    PATH_TO_JSON_FILE = 'model_json_MFCC_345.json'
    PATH_TO_AUDIO = 'audio1.wav'

    tone_detect_obj = ToneDetectionService(PATH_TO_MODEL, PATH_TO_LABELS, PATH_TO_JSON_FILE)
    print(tone_detect_obj.detect_tone(PATH_TO_AUDIO))