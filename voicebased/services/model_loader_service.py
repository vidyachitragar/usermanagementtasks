from keras.models import model_from_json
import pickle
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
# import tensorflow as tf

import keras
# from keras import backend as K

class ModelLoaderService:
    def __init__(self, PATH_TO_WEIGHTS, PATH_TO_LABELS, PATH_TO_JSON_FILE, PATH_TO_MEAN_PICKLE, PATH_TO_STD_PICKLE):

        with open(PATH_TO_LABELS, 'rb') as f:
            self.label = pickle.load(f)

        with open(PATH_TO_JSON_FILE, 'r') as json_file:
            loaded_model_json = json_file.read()

        self.model = model_from_json(loaded_model_json)

        self.model.load_weights(PATH_TO_WEIGHTS)

        optimizer = keras.optimizers.rmsprop(lr=0.00001, decay=1e-6)

        self.model.compile(loss='categorical_crossentropy',
                           optimizer=optimizer,
                           metrics=['accuracy'])

        self.model._make_predict_function()

        with open(PATH_TO_MEAN_PICKLE, "rb") as f:
            self.mean = pickle.load(f)

        with open(PATH_TO_STD_PICKLE, "rb") as f:
            self.std = pickle.load(f)

    def get_label_instance(self):
        return self.label

    def get_model_instance(self):
        return self.model

    def get_mean_instance(self):
        return self.mean

    def get_std_instance(self):
        return self.std