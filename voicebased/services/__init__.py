from voicebased.services.model_loader_service import ModelLoaderService
from voicebased.apps import VoicebasedConfig
from voicebased.services.tone_detection_service_v2 import ToneDetectionService


# Gender Classifier
gender_model_loader_obj = ModelLoaderService(VoicebasedConfig.PATH_TO_GENDER_MODEL,
                                      VoicebasedConfig.PATH_TO_GENDER_LABELS,
                                      VoicebasedConfig.PATH_TO_GENDER_JSON_FILE,
                                      VoicebasedConfig.PATH_TO_GENDER_MEAN,
                                      VoicebasedConfig.PATH_TO_GENDER_STD)

gender_label_obj = gender_model_loader_obj.get_label_instance()
gender_model_obj = gender_model_loader_obj.get_model_instance()
gender_mean_obj = gender_model_loader_obj.get_mean_instance()
gender_std_obj = gender_model_loader_obj.get_std_instance()
gender_detect_obj = ToneDetectionService(gender_label_obj, gender_model_obj, gender_mean_obj, gender_std_obj)


# Female tone Classifier
female_model_loader_obj = ModelLoaderService(VoicebasedConfig.PATH_TO_FEMALE_MODEL,
                                      VoicebasedConfig.PATH_TO_FEMALE_LABELS,
                                      VoicebasedConfig.PATH_TO_FEMALE_JSON_FILE,
                                      VoicebasedConfig.PATH_TO_FEMALE_MEAN,
                                      VoicebasedConfig.PATH_TO_FEMALE_STD)

female_label_obj = female_model_loader_obj.get_label_instance()
female_model_obj = female_model_loader_obj.get_model_instance()
female_mean_obj = female_model_loader_obj.get_mean_instance()
female_std_obj = female_model_loader_obj.get_std_instance()
female_tone_detect_obj = ToneDetectionService(female_label_obj, female_model_obj, female_mean_obj, female_std_obj)


# Male tone Classifier
male_model_loader_obj = ModelLoaderService(VoicebasedConfig.PATH_TO_MALE_MODEL,
                                      VoicebasedConfig.PATH_TO_MALE_LABELS,
                                      VoicebasedConfig.PATH_TO_MALE_JSON_FILE,
                                      VoicebasedConfig.PATH_TO_MALE_MEAN,
                                      VoicebasedConfig.PATH_TO_MALE_STD)

male_label_obj = male_model_loader_obj.get_label_instance()
male_model_obj = male_model_loader_obj.get_model_instance()
male_mean_obj = male_model_loader_obj.get_mean_instance()
male_std_obj = male_model_loader_obj.get_std_instance()
male_tone_detect_obj = ToneDetectionService(male_label_obj, male_model_obj, male_mean_obj, male_std_obj)
