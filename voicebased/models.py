from django.db import models
from django.core.validators import FileExtensionValidator, MaxValueValidator, MinValueValidator
import os


class AudioFile(models.Model):
    audio_file = models.FileField(blank=False, null=False,
                                  upload_to=os.path.join('voicebased', 'input'),
                                  validators=[FileExtensionValidator(["wav"])])

    chunk_seconds = models.PositiveIntegerField(validators=[MinValueValidator(2), MaxValueValidator(4)])

    def __str__(self):
        return self.audio_file.name