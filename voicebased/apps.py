from django.apps import AppConfig
import os
from ml_platform.settings import BASE_DIR


class VoicebasedConfig(AppConfig):
    name = 'voicebased'
    version = 'v4'

    # Path to gender classifier model
    PATH_TO_GENDER_MODEL = os.path.join(BASE_DIR, 'voicebased', 'resources', version , 'saved_models', 'Gender_Model.h5')

    # Path to gender classifier labels
    PATH_TO_GENDER_LABELS = os.path.join(BASE_DIR, 'voicebased', 'resources', version, 'saved_labels', 'gender_labels')

    # Path to gender classifier json file
    PATH_TO_GENDER_JSON_FILE = os.path.join(BASE_DIR, 'voicebased', 'resources', version, 'saved_json_file', 'gender_model_json.json')

    PATH_TO_GENDER_MEAN = os.path.join(BASE_DIR, 'voicebased', 'resources', version, 'mean', 'mean_.pickle')
    PATH_TO_GENDER_STD = os.path.join(BASE_DIR, 'voicebased', 'resources', version, 'std', 'std_.pickle')


    # Path to female classifier model
    PATH_TO_FEMALE_MODEL = os.path.join(BASE_DIR, 'voicebased', 'resources', version, 'saved_models', 'Female_Emotion_Model.h5')

    # Path to female classifier labels
    PATH_TO_FEMALE_LABELS = os.path.join(BASE_DIR, 'voicebased', 'resources', version, 'saved_labels', 'female_emotion_labels')

    # Path to female classifier json file
    PATH_TO_FEMALE_JSON_FILE = os.path.join(BASE_DIR, 'voicebased', 'resources', version, 'saved_json_file', 'Female_emotion_model_json.json')

    PATH_TO_FEMALE_MEAN = os.path.join(BASE_DIR, 'voicebased', 'resources', version, 'mean', 'mean_female.pickle')
    PATH_TO_FEMALE_STD = os.path.join(BASE_DIR, 'voicebased', 'resources', version, 'std', 'std_female.pickle')


    # Path to male classifier model
    PATH_TO_MALE_MODEL = os.path.join(BASE_DIR, 'voicebased', 'resources', version, 'saved_models', 'male_Emotion_Model.h5')

    # Path to male classifier labels
    PATH_TO_MALE_LABELS = os.path.join(BASE_DIR, 'voicebased', 'resources', version, 'saved_labels', 'male_emotion_labels')

    # Path to male classifier json file
    PATH_TO_MALE_JSON_FILE = os.path.join(BASE_DIR, 'voicebased', 'resources', version, 'saved_json_file', 'male_emotion_model_json.json')

    PATH_TO_MALE_MEAN = os.path.join(BASE_DIR, 'voicebased', 'resources', version, 'mean', 'mean_male.pickle')
    PATH_TO_MALE_STD = os.path.join(BASE_DIR, 'voicebased', 'resources', version, 'std', 'std_male.pickle')