import nltk

def main():
    """
    Function to download trained models in nltk.
    """
    trained_models = ['punkt', 'wordnet', 'stopwords', 'averaged_perceptron_tagger']
    for trained_model in trained_models:
        nltk.download(str(trained_model))
        print("{} model has been downloaded!".format(str(trained_model)))

if __name__ == "__main__":
    main()