
from account import views
from django.urls import path, include, re_path

# from rest_framework.routers import DefaultRouter

# router = DefaultRouter()
# router.register('login-viewset', views.LoginViewSet, base_name='login-viewset')


urlpatterns = [

    path('createuser/',views.CreateUserView.as_view()),
    path('login/',views.LoginUserView.as_view()),

]

