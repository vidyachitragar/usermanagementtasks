# import datetime
#
# from django.conf import settings
# from django.db import models
# from django.contrib import auth
# from django.utils.translation import ugettext as _, ungettext, ugettext_lazy
#
#
# import utils
#
#
# # Create your models here.
# class Transaction(models.Model):
#     timestamp = models.DateTimeField(auto_now_add=True, editable=False)
#     subscription = models.ForeignKey('subscription.Subscription',
#                                      null=True, blank=True, editable=False,on_delete=models.CASCADE)
#     user = models.ForeignKey(auth.models.User,
#                              null=True, blank=True, editable=False,on_delete=models.CASCADE)
#
#     class Meta:
#         ordering = ('-timestamp',)
#
#
# _recurrence_unit_days = {
#     'D': 1.,
#     'W': 7.,
#     'M': 30.4368,                      # http://en.wikipedia.org/wiki/Month#Julian_and_Gregorian_calendars
#     'Y': 365.2425,                     # http://en.wikipedia.org/wiki/Year#Calendar_year
#     }
#
# _TIME_UNIT_CHOICES = (
#     ('0', ugettext_lazy('No trial')),
#     ('D', ugettext_lazy('Day')),
#     ('W', ugettext_lazy('Week')),
#     ('M', ugettext_lazy('Month')),
#     ('Y', ugettext_lazy('Year')),
#     )
#
#
#
#
#
#
#
# class Subscription(models.Model):
#     service_name = models.CharField(max_length=100, unique=True, null=False)
#     trial_period = models.PositiveIntegerField(null=True, blank=True)
#     #trial_unit = models.CharField(max_length=1, null=True, choices=_TIME_UNIT_CHOICES)
#     #recurrence_period = models.PositiveIntegerField(null=True, blank=True)
#     #recurrence_unit = models.CharField(max_length=1, null=True,
#                                        #choices=((None, ugettext_lazy("No recurrence")),)
#                                        #+ _TIME_UNIT_CHOICES)
#     #group = models.ForeignKey(auth.models.Group, null=False, blank=False, unique=False,on_delete=models.CASCADE)
#
#     _PLURAL_UNITS = {
#         '0': ugettext_lazy('No trial'),
#         'D': 'days',
#         'W': 'weeks',
#         'M': 'months',
#         'Y': 'years',
#         }
#
#     def __unicode__(self):
#         return self.name
#
#
#
#     def get_trial_display(self):
#         if self.trial_period:
#             return ungettext('One %(unit)s',
#                              '%(period)d %(unit_plural)s',
#                              self.trial_period) % {
#                 'unit': self.get_trial_unit_display().lower(),
#                 'unit_plural': _(self._PLURAL_UNITS[self.trial_unit],),
#                 'period': self.trial_period,
#             }
#         else:
#             return _("No trial")
#
#
#     def save(self, *args, **kwargs):
#         """
#         Set trial period to 0 if the trial unit is 0
#         """
#         if self.trial_unit == "0":
#             self.trial_period = 0
#
#         super(Subscription, self).save(*args, **kwargs)
#
#
#     def __user_get_subscription(user):
#         if not hasattr(user, '_subscription_cache'):
#             sl = Subscription.objects.filter(group__in=user.groups.all())[:1]
#         if sl:
#             user._subscription_cache = sl[0]
#         else:
#             user._subscription_cache = None
#         return user._subscription_cache

#auth.models.User.add_to_class('get_subscription', __user_get_subscription)



# class UserSubscription(models.Model):
#     user = models.ForeignKey(auth.models.User,on_delete=models.CASCADE)
#     subscription = models.ForeignKey(Subscription,on_delete=models.CASCADE)
#     expires = models.DateField(null=True, default=datetime.date.today)
#     active = models.BooleanField(default=True)
#
#
#
#
#     class Meta:
#         unique_together = (('user', 'subscription'), )
#
#     def subscribe(self):
#         """Subscribe user."""
#         self.user.groups.add(self.subscription.group)
#         self.user.save()
#
#     def unsubscribe(self):
#         """Unsubscribe user."""
#         self.user.groups.remove(self.subscription.group)
#         self.user.save()
#
#     def extend(self, timedelta=None):
#         """Extend subscription by `timedelta' or by subscription's
#         recurrence period."""
#         if timedelta is not None:
#             self.expires += timedelta
#         else:
#             if self.subscription.recurrence_unit:
#                 self.expires = utils.extend_date_by(
#                     self.expires,
#                     self.subscription.recurrence_period,
#                     self.subscription.recurrence_unit)
#             else:
#                 self.expires = None

