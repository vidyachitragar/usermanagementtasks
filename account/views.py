from rest_framework.views import APIView
from account.serializers import UserSerializer
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from rest_framework import viewsets
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from rest_framework.throttling import ScopedRateThrottle

#Auth
from rest_framework.authtoken.models import Token


class CreateUserView(APIView):
    serializer_class = UserSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            user = User.objects.create(
            username = serializer.validated_data.get('username'),
            email = serializer.validated_data.get('email'),
            first_name = serializer.validated_data.get('first_name'),
            last_name = serializer.validated_data.get('last_name')

            )
            user.set_password(serializer.validated_data.get('password'))
            user.save()
            return Response("User Created Successfully", status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)


class LoginUserView(APIView):
    # code to handle throttling----------------------------------------------------
    # below code will not allow user to login and access the application if he exceeds the limit
    throttle_classes = (ScopedRateThrottle,)
    throttle_scope = 'user'
    # ----------------------------------------------------

    def post(self, request):
        username = request.data['username']
        password = request.data['password']
        print(username,password)
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request,user)
                token = Token.objects.create(user=user)
                return Response({'Token': str(token.key)}, status=status.HTTP_201_CREATED)
            else:
                return HttpResponse("Your account was inactive.")
        else:
            print("Someone tried to login and failed.")
            print("They used username: {} and password: {}".format(username,password))
            return HttpResponse("Invalid login details given")
