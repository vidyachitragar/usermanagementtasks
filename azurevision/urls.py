from django.conf.urls import url
from .views import AnalyzeImageView, DetectObjectView

urlpatterns = [
    url('analyze_image/',AnalyzeImageView.as_view(),name="file-upload"),
    url('object_detection/',DetectObjectView.as_view(),name="file-upload"),
]