from django.apps import AppConfig
import os
from ml_platform.settings import BASE_DIR

class AzureVisionConfig(AppConfig):
    name = 'azurevision'

    input_path = os.path.join(BASE_DIR, 'media', 'azurevision', 'input')
    output_path = os.path.join(BASE_DIR, 'media', 'azurevision', 'output')
