from django.db import models

import os

class File(models.Model):
    file = models.FileField(blank=False, null=False, upload_to=os.path.join('azurevision', 'input'))

    def __str__(self):
        return self.file.name