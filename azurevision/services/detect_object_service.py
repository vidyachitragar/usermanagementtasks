import requests
import cv2
import os
from azurevision.apps import AzureVisionConfig


class DetectObjectService:

    def __init__(self, image_path):
        self.image_path = image_path
        self.image_name = image_path.split('/')[-1].split('.')[0]
        
    
    def object_detection(self):
        subscription_key = "8e6bd5565efb4766935901a8aa2eb156"
        assert subscription_key
        post_request_url = "https://centralindia.api.cognitive.microsoft.com/vision/v2.0/"
        Azure_post_api_type = "detect/"
        text_recognition_url = post_request_url + Azure_post_api_type
        params = {'language':'en'}
        headers = {'Ocp-Apim-Subscription-Key': subscription_key, 'Content-Type': 'application/octet-stream'}
        bin_image = open(self.image_path, "rb").read()
        data = bin_image
    
        response = requests.post(
                text_recognition_url, headers=headers, params=params, data = data)
        response.raise_for_status()
        data = response.json()
        final_res = []
        final_res.append(data)
        object_info = final_res[0]['objects']
        #print("\n\n\nobject_info :::",object_info)


        bounding_box_list = []
        for bounding_box in object_info:
            bound_box = bounding_box['rectangle']
            bounding_box_list.append(bound_box)

        img = cv2.imread(self.image_path)
        for i in range(len(bounding_box_list)):
            bound_box = bounding_box_list[i]
            crop_img = img[bound_box['y']:bound_box['y'] + bound_box['h'],
                       bound_box['x']:bound_box['w'] + bound_box['x']]
            cropped_image_path = os.path.join(AzureVisionConfig.output_path,self.image_name +"_{0}.jpg".format(i))
            cv2.imwrite(cropped_image_path, crop_img)


        output_list = []
        output_dict = {}
        count = 0

        for i in range(len(object_info)):
            temp_dict = dict()
            object = object_info[i]['object']
            confidence = object_info[i]['confidence']
            temp_dict['object'] = object
            temp_dict['confidence'] = confidence
            # print(output_dict)
            count = count + 1
            output_list.append(temp_dict)
        output_dict['no_of_objects'] = count
        output_dict['objects'] = output_list

        return output_dict


if __name__ == "__main__":
    image_path = "car.jpg"
    #image_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQaWxdPBf2rV8VAFl0zRHEi4iT0Rsd6iKR2nVLfqTEDo6oxO-JXZg"
    obj = DetectObjectService(image_path)
    result = obj.object_detection()
