import requests
       
class AnalyzeImageService:

    def __init__(self, image_path):
        self.image_path = image_path
        
    
    def analyze_image(self):
        subscription_key = "8e6bd5565efb4766935901a8aa2eb156"
        assert subscription_key
        post_request_url = "https://centralindia.api.cognitive.microsoft.com/vision/v2.0/"
        Azure_post_api_type = "analyze/"
        text_recognition_url = post_request_url + Azure_post_api_type
        params = {'visualFeatures': 'Description'}
        headers = {'Ocp-Apim-Subscription-Key': subscription_key, 'Content-Type': 'application/octet-stream'}
        bin_image = open(self.image_path, "rb").read()
        data = bin_image
    
        response = requests.post(
                text_recognition_url, headers=headers, params=params, data=data)
        response.raise_for_status()
        data = response.json()
        final_res = list()
        final_res.append(data)
        for value in final_res:
            text = value['description']['captions'][0]['text'] 
        return text


if __name__ == "__main__":
    image_path = "car.jpg"
    # bin_image = open(image_path, "rb").read()
    obj = AnalyzeImageService(image_path)
    text = obj.analyze_image()
