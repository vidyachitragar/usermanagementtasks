from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import FileUploadParser
from rest_framework import status
from .serializers import FileSerializer
from azurevision.services.analyze_image_service import AnalyzeImageService
from azurevision.services.detect_object_service import DetectObjectService

class AnalyzeImageView(APIView):
    parser_class = (FileUploadParser,)

    def post(self, request):
        file_serializer = FileSerializer(data=request.data)
        if file_serializer.is_valid():
            file_serializer.save()
            file_path = file_serializer.data.get('file')

            analyze_image_obj=AnalyzeImageService(file_path)
            text = analyze_image_obj.analyze_image()

            return Response(text, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetectObjectView(APIView):
    parser_class = (FileUploadParser,)

    def post(self, request):
        file_serializer = FileSerializer(data=request.data)
        if file_serializer.is_valid():
            file_serializer.save()
            file_path = file_serializer.data.get('file')

            detect_object_obj=DetectObjectService(file_path)
            resp_dict = detect_object_obj.object_detection()

            return Response(resp_dict, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)