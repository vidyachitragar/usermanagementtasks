from django.db import models
from django.contrib.auth.models import User



# Create your models here.
class AnalyticsDataBase(models.Model):
    """Database model for analysis and statistics"""
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    service_name = models.CharField(max_length=255)
    time = models.DateTimeField(auto_now_add=True)
    app_name = models.CharField(max_length=255)



    # @classmethod
    # def create(cls, service_name,app_name):
    #     AnalyticsDataBase = cls(service_name=service_name,app_name=app_name)
    #     # do something with the book
