from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Count
from analytics.models import AnalyticsDataBase
from django.contrib.auth.models import User
from django.db.models.query import QuerySet
import numpy as np

import json
from django.http import HttpResponse


class UserAnalyticsView(APIView):

    def get(self,request,format=None):
        user_id = request.GET["userid"]

        #Syntax
        #ModelName.objects.values('Colum_name')

        count = len(AnalyticsDataBase.objects.filter(user_id=user_id))
        user_name = User.objects.get(id=user_id)

        service_list_query =list(AnalyticsDataBase.objects.filter(user_id=user_id).values('service_name','user_id'))
        service_name = [ service_list_query[value]['service_name'] for value in range(len(service_list_query)) ]
        values, counts = np.unique(service_name, return_counts=True)


        query = list(AnalyticsDataBase.objects.values('user_id').annotate(Count('service_name')).order_by())
        user_service_details = [ query[value]['service_name__count'] for value in range(len(query)) if str(query[value]['user_id']) == user_id ]

        analytics = [ {"service_name":value,"count":count} for value, count in zip(values, counts) ]

        context = {
            'user_id':user_id,
            'username':user_name,
            'analytics':analytics,
            'total_user_service_count':user_service_details[0]
        }

        data = json.dumps(context, sort_keys=True, default=str)
        return HttpResponse(data, content_type='application/json')
