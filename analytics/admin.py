from django.contrib import admin
from analytics.models import AnalyticsDataBase
# Register your models here.
admin.site.register(AnalyticsDataBase)
