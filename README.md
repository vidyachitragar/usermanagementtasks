# Local setup to execute the project

Pre-requisites -
  - Python 3.6
  - Java 8 (For Tabula)
  - virtualenv (Python package)

Ubuntu installation for the above

```bash
apt-get install -y python3-pip
apt-get install -y python3-venv
apt-get install -y default-jre
apt-get install -y default-jdk

```
### Create Virtual environment

Make sure Python 3.6 is installed. (Do not install Anaconda Python).

```bash
python -m venv ml-pltaform-venv

```

To activate the virtual environment, execute the `activate.bat` file in `ml-platform-venv/Scripts/`

Run,

```bash
ml-platform-venv/Scripts/activate

```
This would activate the virtual environment.

### Clone the repository

To clone the repository,
```bash
git clone https://vishalsain@bitbucket.org/vishalsain/ml_platform_development.git

```


### Install dependencies

Navigate inside the project directory
```bash
cd ml_platform

```

Once the virtual env is activated, install the required dependencies from
```bash
pip install -r requirements_2.txt

```

To download `nltk` dependencies, run
```bash
python install_nltk_dependencies.py

```

To download `spacy`'s language models, run
```bash
python -m spacy download "en_core_web_sm"

```

### Get model files

This would get all the files required except the model files, which have been uploaded to Google Drive.

To get the model files from Google Drive,
and run
```bash
python get_model_files.py

```

### Usage

To run the server, make sure you are in the `ml_platform` directory and execute

```bash
python manage.py makemigrations
python manage.py migrate
python manage.py runserver

```

### Solution implemented for the purpose

Implemented the throttling to limit the user login access after he/she access the limit

once user exceeds his limit django-trottle throws error message such as '''{"detail": "Request was throttled. Expected available in 86274 seconds."}'''
as "day" is setted in the settings.py file while configuring the throttling policies

ex: 20/day


#------------------------------------------------------------------------------------------------------

