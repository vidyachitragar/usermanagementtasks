from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, generics
from rest_framework.parsers import FileUploadParser
from .serializers import TextSerializer, FileSerializer, PDFFileSerializer, AudioFileSerializer, DocumentSerializer
import numpy as np
from epiphany.services.text_classification_service import TextClassifier
from epiphany.services.pdf_extract_service import TabulaTextExtractor
from epiphany.services.summarize_service import ExtractiveSummarization
from epiphany.utils.model_loader import ModelLoader
from epiphany.services.azure_speech_to_text_service import SpeechToTextAzureService
from epiphany.services.google_speech_to_text_service import SpeechToTextGoogleService
from epiphany.services.docx_to_text_service import DocxToTextService
from commons.file_handling import create_response

#Auth
from rest_framework.authentication import SessionAuthentication, BasicAuthentication ,TokenAuthentication
from rest_framework.permissions import IsAuthenticated


import logging
logger = logging.getLogger(__name__)

# class FrontendRenderView(APIView):
#
#     def get(self, request, *args, ** kwargs):
#         return render(request, "index.html", {})


class SpeechToTextView(APIView):

    parser_class = (FileUploadParser,)

    def post(self, request):

        audio_file_serializer = AudioFileSerializer(data=request.data)
        if audio_file_serializer.is_valid():
            audio_file_serializer.save()

            audio_file_path = audio_file_serializer.data.get('speech')
            service_flag = audio_file_serializer.data.get('service')

            if service_flag == "1":
                speech_to_text_obj = SpeechToTextAzureService(audio_file_path)
                converted_text = speech_to_text_obj.post_request()

            else:
                speech_to_text_obj = SpeechToTextGoogleService(audio_file_path)
                converted_text = speech_to_text_obj.speech_to_text()

            response_dict = create_response({'extracted_text': converted_text},
                                            "Successful")

            return Response(response_dict, status=status.HTTP_201_CREATED)

        # except Exception as e:
        #     #logger
        #     return Response({"data":None,"message":"Something went wrong"})
        else:
            response_dict = create_response(None, audio_file_serializer.errors.get("speech")[0])
            return Response(response_dict, status=status.HTTP_400_BAD_REQUEST)


class TextClassificationView(APIView):

    #Auth
    permission_classes = (IsAuthenticated,) 
    authentication_classes = (TokenAuthentication,)


    def post(self, request):
        # base_url = "{0}://{1}{2}".format(request.scheme, request.get_host(), request.path)
        modeling_type = request.path.split('/')[-3]
        text_serializer = TextSerializer(data=request.data)

        if text_serializer.is_valid():
            text_serializer.save()
            # service class goes here
            input_text = text_serializer.data.get('text')
            if modeling_type == "sentiment":
                model_loader_obj = ModelLoader()
                model_instance = model_loader_obj.get_sentiment_model()
                vocab_instance = model_loader_obj.get_sentiment_vocabulary()
                class_map_instance = model_loader_obj.get_sentiment_map()
                logger.info("{} has been loaded!".format(modeling_type))

            elif modeling_type == "spam":
                model_loader_obj = ModelLoader()
                model_instance = model_loader_obj.get_spam_model()
                vocab_instance = model_loader_obj.get_spam_vocabulary()
                class_map_instance = model_loader_obj.get_spam_map()
                logger.info("{} has been loaded!".format(modeling_type))

            elif modeling_type == "infosec":
                model_loader_obj = ModelLoader()
                model_instance = model_loader_obj.get_infosec_model()
                vocab_instance = model_loader_obj.get_infosec_vocabulary()
                class_map_instance = model_loader_obj.get_infosec_map()

                logger.info("{} has been loaded!".format(modeling_type))
            # try:
            #     classification_obj = TextClassifier(model_instance,
            #                                         vocab_instance,
            #                                         class_map_instance)
            #     prediction = classification_obj.predict_class(input_text)
            #     logger.info("Classification Worked!")
            # except:
            #     logger.error("TextClassification did not work!")
            #     prediction = ''
            classification_obj = TextClassifier(model_instance,
                                                vocab_instance,
                                                class_map_instance)
            prediction = classification_obj.predict_class(input_text)
            logger.info("Classification Worked!")

            response_dict = {
                "data": {"text": input_text,
                         "predicted_class": prediction},
                # "meta": {"feature": modeling_type,
                #          "version": EpiphanyConfig.version},
                "message": "Success"
            }
            return Response(response_dict, status=status.HTTP_201_CREATED)
        else:
            response_dict = {
                "data": None,
                "message": text_serializer.errors.get("text")[0]
            }
            return Response(response_dict, status=status.HTTP_400_BAD_REQUEST)

class PDFExtractView(APIView):

    parser_class = (FileUploadParser,)

    def post(self, request):
        pdffile_serializer = PDFFileSerializer(data=request.data)

        if pdffile_serializer.is_valid():
            pdffile_serializer.save()

            file_path = pdffile_serializer.data.get('pdf_file')

            extractor_obj = TabulaTextExtractor(file_path)
            # ext_obj.display_text()
            extracted_text = extractor_obj.extract_text()

            # Creating Response
            response_dict = create_response({"extracted_text": extracted_text}, "Success")

            return Response(response_dict, status=status.HTTP_201_CREATED)
        else:
            response_dict = create_response(None, pdffile_serializer.errors.get("pdf_file")[0])

            return Response(response_dict, status=status.HTTP_400_BAD_REQUEST)

class DocumentExtractView(APIView):

    parser_class = (FileUploadParser,)

    def post(self, request):
        doc_serializer = DocumentSerializer(data=request.data)

        if doc_serializer.is_valid():
            doc_serializer.save()

            file_path = doc_serializer.data.get('doc_file')

            extractor_obj = DocxToTextService(file_path)
            extracted_text = extractor_obj.get_docx_text()

            # Creating Response
            response_dict = create_response({"extracted_text": extracted_text}, "Success")

            return Response(response_dict, status=status.HTTP_201_CREATED)
        else:
            response_dict = create_response(None, doc_serializer.errors.get("doc_file")[0])

            return Response(response_dict, status=status.HTTP_400_BAD_REQUEST)

class SummarizationView(APIView):

    def post(self, request):
        file_serializer = FileSerializer(data=request.data)
        summarize_percent = request.POST['percent']

        if file_serializer.is_valid():
            file_serializer.save()

            file_path = file_serializer.data.get('file')
            # get text from file
            with open(file_path, 'r') as f:
                document = f.read()

            summarize_obj = ExtractiveSummarization(np.float(summarize_percent))
            summary = summarize_obj.summarize(document)
            sentences_in_document, sentences_in_summary = summarize_obj.get_lengths()

            # Creating Response
            response_dict = create_response({"text": document,
                                             "summary": summary,
                                             "sentences_in_document": sentences_in_document,
                                             "sentences_in_summary": sentences_in_summary}, "Success")

            return Response(response_dict, status=status.HTTP_201_CREATED)
        else:
            response_dict = create_response(None, file_serializer.errors.get("file")[0])

            return Response(response_dict, status=status.HTTP_400_BAD_REQUEST)