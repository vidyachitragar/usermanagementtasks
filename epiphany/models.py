from django.db import models
from django.core.validators import FileExtensionValidator
import os

class Text(models.Model):
    text = models.CharField(max_length=1200)

    def __str__(self):
        return self.text

class File(models.Model):
    file = models.FileField(blank=False,
                            null=False,
                            upload_to=os.path.join('epiphany', 'summarization'),
                            validators=[FileExtensionValidator(["txt"])])

    def __str__(self):
        return self.file.name

class AudioFile(models.Model):
    speech = models.FileField(blank=False, null=False,
                              upload_to='epiphany',
                              validators=[FileExtensionValidator(["wav"])])
    service = models.CharField(max_length=1, blank=True)

    def __str__(self):
        return self.speech.name


class PDFFile(models.Model):
    pdf_file = models.FileField(blank=False,
                                null=False,
                                upload_to=os.path.join('epiphany', 'text_extract'),
                                validators=[FileExtensionValidator(["pdf"])])

    def __str__(self):
        return self.pdf_file.name

class Document(models.Model):
    doc_file = models.FileField(blank=False,
                                null=False,
                                upload_to=os.path.join('epiphany', 'text_extract'),
                                validators=[FileExtensionValidator(["doc", "docx"])])

    def __str__(self):
        return self.doc_file.name