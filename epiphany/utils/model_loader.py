import pickle
from epiphany.apps import EpiphanyConfig

class ModelLoader():
    """
    Loading trained model and vocabulary as instance which can be called later
    """
    def __init__(self):
        # Sentiment Paths
        with open(EpiphanyConfig.sentiment_model_path, 'rb') as fp:
            self.sentiment_model = pickle.load(fp)

        with open(EpiphanyConfig.sentiment_vocabulary_path, 'rb') as fp:
            self.sentiment_vocabulary = pickle.load(fp)

        # Spam Paths
        with open(EpiphanyConfig.spam_model_path, 'rb') as fp:
            self.spam_model = pickle.load(fp)

        with open(EpiphanyConfig.spam_vocabulary_path, 'rb') as fp:
            self.spam_vocabulary = pickle.load(fp)

        # Infosec Paths
        with open(EpiphanyConfig.infosec_model_path, 'rb') as fp:
            self.infosec_model = pickle.load(fp)

        with open(EpiphanyConfig.infosec_vocabulary_path, 'rb') as fp:
            self.infosec_vocabulary = pickle.load(fp)

        # Define Class maps
        self.sentiment_map = {0: 'neutral', 1: "positive", 2: 'negative'}
        self.spam_map = {0: 'ham', 1: "spam"}
        self.infosec_map = {0: "no-threat", 1: "threat"}

    def get_sentiment_model(self):
        return self.sentiment_model

    def get_sentiment_vocabulary(self):
        return self.sentiment_vocabulary

    def get_sentiment_map(self):
        return self.sentiment_map

    def get_spam_model(self):
        return self.spam_model

    def get_spam_vocabulary(self):
        return self.spam_vocabulary

    def get_spam_map(self):
        return self.spam_map

    def get_infosec_model(self):
        return self.infosec_model

    def get_infosec_vocabulary(self):
        return self.infosec_vocabulary

    def get_infosec_map(self):
        return self.infosec_map
