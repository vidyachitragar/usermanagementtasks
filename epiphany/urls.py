from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from .views import PDFExtractView, SummarizationView, TextClassificationView, SpeechToTextView, DocumentExtractView
# from .views import SentimentView, SpamView

urlpatterns = [
    path('sentiment/text/', TextClassificationView.as_view()),
    path('spam/text/', TextClassificationView.as_view()),
    path('infosec/text/', TextClassificationView.as_view()),
    path('asr/', SpeechToTextView.as_view()),
    path('extract/pdf/', PDFExtractView.as_view()),
    path('extract/doc/', DocumentExtractView.as_view()),
    path('summary/file/', SummarizationView.as_view()),
    # path('summary/file/', SummarizationView.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)