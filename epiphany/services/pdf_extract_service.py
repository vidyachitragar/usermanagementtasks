import tabula
import logging
logger = logging.getLogger(__name__)

class TabulaTextExtractor:

    def __init__(self, path_to_pdf):
        self.path_to_pdf = path_to_pdf

    def _load_text(self):
        """
        Function to extract text from PDFs.

        Return:
             A dictionary with co-ordinates of text and the text itself.
        """
        try:
            extracted_text_dict = tabula.read_pdf(self.path_to_pdf,
                                                  output_format="json",
                                                  guess=False,
                                                  pages='all')
            logger.info("Loaded pdf text as python dictionary!")
            return extracted_text_dict
        except Exception as e:
            print(e)
            logger.error("Couldn't load PDF")

    def display_text(self):
        """
        Function to print text from the entire PDF to the console.
        Loops through all_text_dictionary and prints the text in key "data".
        """
        all_text = self._load_text()
        for each_page_dict in all_text:
            for each_line_dict in each_page_dict['data']:
                print(each_line_dict[0]['text'])

    def extract_text(self):
        """
        Function to extract text data from PDF.

        Return:
            String containing all the text in the PDF, concatenated.
        """
        entire_doc_dict = self._load_text()
        all_text = ""
        for each_page_dict in entire_doc_dict:
            for each_line_dict in each_page_dict['data']:
                all_text += "\n" + each_line_dict[0]['text']
        if all_text == '':
            logger.warning("Couldn't extract any text!")
        else:
            pass
        return all_text

if __name__ == "__main__":

    PATH_TO_PDF = 'media/pdf_depositslip.pdf'
    ext_obj = TabulaTextExtractor(PATH_TO_PDF)
    # ext_obj.display_text()
    text = ext_obj.extract_text()
    print(text)