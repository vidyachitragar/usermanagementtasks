from sklearn.feature_extraction.text import TfidfVectorizer
from nltk import RegexpTokenizer
from nltk.tokenize import word_tokenize
import re
from epiphany.apps import EpiphanyConfig

class TextClassifier(object):

    def __init__(self, model, vocabulary, class_map):
        self.model_instance = model
        self.vocabulary = vocabulary
        self.builtinstopwords = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', "you're", "you've",
                                 'hi', 'hey', 'hello']
        self.ReverseReplacement = class_map

    def _clean_text(self, text):
        tokenized_text = word_tokenize(text)
        cleaned_text = " ".join([t for t in tokenized_text if re.match('[a-zA-Z\-][a-zA-Z\-]{2,}', t)])
        cleaned_text = cleaned_text.lower()
        e = RegexpTokenizer(r'[a-zA-Z]+').tokenize(cleaned_text)
        filtered_sentence = [w for w in e if not w in self.builtinstopwords]
        return " ".join(filtered_sentence)

    def predict_class(self, text):

        cleaned_text = self._clean_text(text)

        tfidf_obj = TfidfVectorizer(vocabulary=self.vocabulary)
        X = tfidf_obj.fit_transform([cleaned_text])
        predicted_value = self.model_instance.predict(X)
        for key in self.ReverseReplacement.keys():
            if key == predicted_value[0]:
                prediction = self.ReverseReplacement[key]

        return prediction

if __name__ == "__main__":

    sentiment_obj = TextClassifier(EpiphanyConfig.sentiment_model_path,
                                   EpiphanyConfig.sentiment_vocabulary_path)
    predicted_value = sentiment_obj.predict_sentiment("I don't know what to say!")
    print(predicted_value)