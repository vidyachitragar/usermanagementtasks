import speech_recognition as sr
from os import path
class SpeechToTextGoogleService():
    
    
    def __init__(self, PATH_TO_AUDIO_FILE):
        self.PATH_TO_AUDIO_FILE = PATH_TO_AUDIO_FILE
        
    def speech_to_text(self):
        # use the audio file as the audio source
        r = sr.Recognizer()
        with sr.AudioFile(self.PATH_TO_AUDIO_FILE) as source:
            audio = r.record(source)  # read the entire audio file
        # recognize speech using Google Speech Recognition
        try:
            # print(r.recognize_google(audio))
            converted_text = r.recognize_google(audio)
            return converted_text
        except sr.UnknownValueError:
            print("Google Speech Recognition could not understand audio")
        except sr.RequestError as e:
            print("Could not request results from Google Speech Recognition service; {0}".format(e))

if __name__ == "__main__":
    AUDIO_FILE = path.join(path.dirname(path.realpath("__file__")), "audio/test/ar-07.wav")
    obj = SpeechToTextGoogleService(AUDIO_FILE)
    result = obj.speech_to_text()