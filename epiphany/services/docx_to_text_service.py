from docx import Document

class DocxToTextService():
    def __init__(self, filepath):
        self.filepath = filepath

    def get_docx_text(self):
        """
        Take the path of a docx file as argument, return the text.
        """  
        doc = Document(self.filepath)
        document_string = ""
        for paragraph in doc.paragraphs:
            document_string += paragraph.text + "\n"

        return document_string