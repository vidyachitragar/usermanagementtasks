import requests
import json

class SpeechToTextAzureService():

    def __init__(self, PATH_TO_AUDIO_FILE):
        self.PATH_TO_AUDIO_FILE = PATH_TO_AUDIO_FILE

    def post_request(self):
        url = 'https://centralindia.stt.speech.microsoft.com/speech/recognition/conversation/cognitiveservices/v1?language=en-US'

        payload = open(self.PATH_TO_AUDIO_FILE, 'rb').read()
        headers = {
          'Accept': 'application/json',
          'Content-Type': 'audio/wav',
          'Ocp-Apim-Subscription-Key': '8e6bd5565efb4766935901a8aa2eb156',
        }
        response = requests.request('POST', url, headers=headers, data=payload)
        converted_text_dict = json.loads(response.text)

        converted_text = converted_text_dict.get('DisplayText')
        return converted_text

if __name__ == "__main__":

    # AUDIO_FILE = open('C:/Users/Tanu/Documents/comparison/audio/test/ar-07.wav', 'rb').read()
    # obj=SpeechToTextAzureService(AUDIO_FILE)
    # text = obj.post_request()
    pass
