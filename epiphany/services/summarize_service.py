import re
import string
import numpy as np
from nltk import pos_tag
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.corpus.reader.wordnet import NOUN, VERB, ADJ, ADV
from nltk.cluster.util import cosine_distance
from operator import itemgetter
from math import floor
import networkx as nx
import logging
logger = logging.getLogger(__name__)

class TextCleaner:

    def __init__(self):
        self.stop_words = set(stopwords.words("english"))
        self.punctuations = set(string.punctuation)
        self.pos_tags = {
            NOUN: ['NN', 'NNS', 'NNP', 'NNPS', 'PRP', 'PRP$', 'WP', 'WP$'],
            VERB: ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ'],
            ADJ: ['JJ', 'JJR', 'JJS'],
            ADV: ['RB', 'RBR', 'RBS', 'WRB']
        }

    def _remove_stop_words(self, words):
        return [w for w in words if w not in self.stop_words]

    def _remove_regex(self):
        self.input_sent = " ".join([w.lower() for w in self.input_sent])
        self.input_sent = re.sub(r"i'm", "i am", self.input_sent)
        self.input_sent = re.sub(r"he's", "he is", self.input_sent)
        self.input_sent = re.sub(r"she's", "she is", self.input_sent)
        self.input_sent = re.sub(r"that's", "that is", self.input_sent)
        self.input_sent = re.sub(r"what's", "what is", self.input_sent)
        self.input_sent = re.sub(r"where's", "where is", self.input_sent)
        self.input_sent = re.sub(r"\'ll", " will", self.input_sent)
        self.input_sent = re.sub(r"\'ve", " have", self.input_sent)
        self.input_sent = re.sub(r"\'re", " are", self.input_sent)
        self.input_sent = re.sub(r"\'d", " would", self.input_sent)
        self.input_sent = re.sub(r"won't", "will not", self.input_sent)
        self.input_sent = re.sub(r"can't", "cannot", self.input_sent)
        self.input_sent = re.sub(r"don't", "do not", self.input_sent)
        patterns = re.finditer("#[\w]*", self.input_sent)
        for pattern in patterns:
            self.input_sent = re.sub(pattern.group().strip(), "", self.input_sent)
        self.input_sent = "".join(ch for ch in self.input_sent if ch not in self.punctuations)

    def _tokenize(self):
        return word_tokenize(self.input_sent)

    def _process_content_for_pos(self, words):
        tagged_words = pos_tag(words)
        pos_words = []
        for word in tagged_words:
            flag = False
            for key, value in self.pos_tags.items():
                if word[1] in value:
                    pos_words.append((word[0], key))
                    flag = True
                    break
            if not flag:
                pos_words.append((word[0], NOUN))
        return pos_words

    def _remove_noise(self):
        self._remove_regex()
        words = self._tokenize()
        noise_free_words = self._remove_stop_words(words)
        return noise_free_words

    def _normalize_text(self, words):
        lem = WordNetLemmatizer()
        pos_words = self._process_content_for_pos(words)
        normalized_words = [lem.lemmatize(w, pos=p) for w, p in pos_words]
        return normalized_words

    def clean_up(self, input_sent):
        self.input_sent = input_sent
        cleaned_words = self._remove_noise()
        cleaned_words = self._normalize_text(cleaned_words)
        return cleaned_words


def sentence_similarity(sent1, sent2):
    text_cleaner = TextCleaner()
    sent1 = text_cleaner.clean_up(sent1)

    sent2 = text_cleaner.clean_up(sent2)

    all_words = list(set(sent1 + sent2))

    vector1 = [0] * len(all_words)
    vector2 = [0] * len(all_words)
    # print("vect1",vector1)

    for w in sent1:
        vector1[all_words.index(w)] += 1
    # print("vector1 : ", vector1)
    for w in sent2:
        vector2[all_words.index(w)] += 1
    # print("vector2 : ", vector2)

    return 1 - cosine_distance(vector1, vector2)

# %% Build similarity matrix

def build_similarity_matrix(sentences):
    S = np.zeros((len(sentences), len(sentences)))
    for i in range(len(sentences)):
        for j in range(len(sentences)):
            if i == j:
                continue
            else:
                S[i][j] = sentence_similarity(sentences[i], sentences[j])
                if np.isnan(S[i][j]):
                    S[i][j] = 0.0

    for i in range(len(S)):
        S[i] /= S[i].sum()

    return S


# %% Implementing pagerank algorithm using networkx library

def pagerank(S):
    nx_graph = nx.from_numpy_array(S)
    scores = nx.pagerank_numpy(nx_graph)
    return scores

class ExtractiveSummarization:

    def __init__(self, sumpercent):
        self.sumpercent = sumpercent

    def summarize(self, paragraph):

        self.sentences = sent_tokenize(paragraph)
        # print("length of sentences : ", len(self.sentences))
        if len(self.sentences) < 10 and len(self.sentences) > 4:
            self.SUMMARY_SIZE = 3
        elif len(self.sentences) <= 4:
            self.SUMMARY_SIZE = 1
        else:
            self.SUMMARY_SIZE = floor(self.sumpercent * (len(self.sentences) / 100))

        # print("\nThe Summary will contain {} sentences".format(self.SUMMARY_SIZE) + "\n")

        # print("Building Similarity Matrix!\n")

        S = build_similarity_matrix(self.sentences)
        # print("Similarity Matrix Built!\n")
        logger.info("Similarity Matrix Built")

        sentence_ranks = pagerank(S)
        sentence_ranks = [sentence_ranks[key] for key in sentence_ranks]
        # print("PageRank algorithm applied\n")

        ranked_sentence_indexes = [item[0] for item in sorted(enumerate(sentence_ranks), key=lambda item: -item[1])]
        # print("Sentences Ranked!\n")
        logger.info("Sentences Ranked!")

        # global selected_sentence_indexes
        selected_sentence_indexes = sorted(ranked_sentence_indexes[:self.SUMMARY_SIZE])

        # print("Printing results\n")

        selected_sentences = itemgetter(*selected_sentence_indexes)(self.sentences)

        summary = ""
        if self.SUMMARY_SIZE == 1:
            summary = selected_sentences
        else:
            summary += "\n".join(selected_sentences)

        return summary

    def get_lengths(self):
        docsent = len(self.sentences)
        sumsent = self.SUMMARY_SIZE

        return docsent, sumsent

if __name__ == "main":
    pass
    # sum_obj = ExtractiveSummarization(summarize_percent)
    # summary = sum_obj.summarize(paragraph)
    #
    # # Logic to return number of sentences
    # sentences_in_document, sentences_in_summary = sum_obj.get_lengths()