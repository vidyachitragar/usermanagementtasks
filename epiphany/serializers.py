from rest_framework import serializers
from .models import Text, File, PDFFile, AudioFile, Document

class TextSerializer(serializers.ModelSerializer):
    class Meta:
        model = Text
        fields = "__all__"
        # fields = ("text",)

class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = "__all__"

class PDFFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = PDFFile
        fields = "__all__"

class AudioFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = AudioFile
        fields = "__all__"

class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = "__all__"
