# Generated by Django 2.2.5 on 2020-03-03 05:48

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AudioFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('speech', models.FileField(upload_to='epiphany', validators=[django.core.validators.FileExtensionValidator(['wav'])])),
                ('service', models.CharField(blank=True, max_length=1)),
            ],
        ),
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('doc_file', models.FileField(upload_to='epiphany\\text_extract', validators=[django.core.validators.FileExtensionValidator(['doc', 'docx'])])),
            ],
        ),
        migrations.CreateModel(
            name='File',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file', models.FileField(upload_to='epiphany\\summarization', validators=[django.core.validators.FileExtensionValidator(['txt'])])),
            ],
        ),
        migrations.CreateModel(
            name='PDFFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pdf_file', models.FileField(upload_to='epiphany\\text_extract', validators=[django.core.validators.FileExtensionValidator(['pdf'])])),
            ],
        ),
        migrations.CreateModel(
            name='Text',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.CharField(max_length=1200)),
            ],
        ),
    ]
