from django.apps import AppConfig
from ml_platform.settings import BASE_DIR
import os

class EpiphanyConfig(AppConfig):
    name = 'epiphany'
    version = 'v1'

    # sentiment
    sentiment_model_path = os.path.join(name, 'resources', 'sentiment', version, 'sentiment_model.pickle')
    sentiment_vocabulary_path = os.path.join(name, 'resources', 'sentiment', version, 'sentiment_vocabulary.pickle')

    # spam
    spam_model_path = os.path.join(name, 'resources', 'spam', version, 'spam_model.pickle')
    spam_vocabulary_path = os.path.join(name, 'resources', 'spam', version, 'spam_vocabulary.pickle')

    # infosec
    infosec_model_path = os.path.join(name, 'resources', 'infosec', version, 'infosec_model.pickle')
    infosec_vocabulary_path = os.path.join(name, 'resources', 'infosec', version, 'infosec_vocabulary.pickle')