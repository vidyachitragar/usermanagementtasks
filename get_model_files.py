from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from googleapiclient.http import MediaIoBaseDownload
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import io

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/drive']

def main(filename, file_id, folder_name=None):
    """Shows basic usage of the Drive v3 API.
    Prints the names and ids of the first 10 files the user has access to.
    """
    print("\nFetching {}".format(filename))
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('drive', 'v3', credentials=creds)

    # Download the files.
    # file_id = '1y9xrhEEs-Vz_PieYcYK1jeGiFpAGSeK5'
    request = service.files().get_media(fileId=file_id)

    fh = io.BytesIO()
    downloader = MediaIoBaseDownload(fh, request)
    done = False
    while done is False:
        status, done = downloader.next_chunk()
        # print("size of fh", fh.__sizeof__())
        print("Download %d%%." % int(status.progress() * 100))

    # Create folder if it doesn't exist
    write_path = folder_name

    if not os.path.exists(write_path):
        os.makedirs(write_path)

    # Write File to Disk
    with open(os.path.join(write_path, filename), 'wb') as f:
        f.write(fh.getvalue())
    print("{} has been downloaded".format(filename))

def get_file_ids():
    """Shows basic usage of the Drive v3 API.
    Prints the names and ids of the first 10 files the user has access to.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('drive', 'v3', credentials=creds)

    # Call the Drive v3 API
    results = service.files().list(
        pageSize=10, fields="nextPageToken, files(id, name)").execute()
    items = results.get('files', [])

    if not items:
        print('No files found.')
    else:
        print('Files:')
        for item in items:
            print(u'{0} ({1})'.format(item['name'], item['id']))

if __name__ == "__main__":
    # get_file_ids()

    # signature detection models
    main("signature_label_map.pbtxt", "1Z_X09xAhTOXkF0HaKffvXDI7KbM7Slpi", os.path.join('envisage',
                                                                                        'resources',
                                                                                        'signature_detection',
                                                                                        'v1'))
    main("signature_frozen_inference_graph.pb", "18tsdKaWYJNxsIgduQyxwbGSRDrIsrl-D", os.path.join('envisage',
                                                                                                  'resources',
                                                                                                  'signature_detection',
                                                                                                  'v1'))


    # logo detection models
    main("logo_label_map.pbtxt", "1S_DEErFB-GHPpKazWJqwH289_kcyilfy", os.path.join('envisage',
                                                                                   'resources',
                                                                                   'logo_detection',
                                                                                   'v1'))
    main("logo_frozen_inference_graph.pb", "1Tz9fPjbd59OJEA0f_ysBGs5JzookMaoH", os.path.join('envisage',
                                                                                             'resources',
                                                                                             'logo_detection',
                                                                                             'v1'))

    # sentiment models for epiphany
    main("sentiment_model.pickle", "14rlIaoq9UVIcoATycrDfPRjjrH33J-sD", os.path.join('epiphany',
                                                                                     'resources',
                                                                                     'sentiment',
                                                                                     'v1'))
    main("sentiment_vocabulary.pickle", "1uAhRzUrfSKGm0smUDUkE-U1pM1twwaL6", os.path.join('epiphany',
                                                                                          'resources',
                                                                                          'sentiment',
                                                                                          'v1'))

    # spam models
    main("spam_model.pickle", "1nK4hveFrWhJ5uCqog_OLl_XJCxyiJeac", os.path.join('epiphany',
                                                                                'resources',
                                                                                'spam',
                                                                                'v1'))
    main("spam_vocabulary.pickle", "18bbZbLjh_CaWQtvXTYuL0CmomqBFrzyw", os.path.join('epiphany',
                                                                                     'resources',
                                                                                     'spam',
                                                                                     'v1'))

    # infosec models for epiphany
    main("infosec_model.pickle", "1kXygMYzR4_vTyEHOaCeMUTOL-ushMOHj", os.path.join('epiphany',
                                                                                   'resources',
                                                                                   'infosec',
                                                                                   'v1'))
    main("infosec_vocabulary.pickle", "1AfhwHj33v6c05W7gsdSfIk-znZQSWSTg", os.path.join('epiphany',
                                                                                        'resources',
                                                                                        'infosec',
                                                                                        'v1'))
