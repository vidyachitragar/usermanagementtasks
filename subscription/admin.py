from django.contrib import admin
from django.utils.html import conditional_escape as esc

#from subscription.models import Subscription,Transaction,UserSubscription
from subscription.models import SubscribedUser ,Subscription ,SubscriptionType ,SubscriptionService ,Service
# Register your models here.

admin.site.register(SubscribedUser)
admin.site.register(Subscription)
admin.site.register(SubscriptionType)
admin.site.register(SubscriptionService)
admin.site.register(Service)



#
# class SubscriptionAdmin(admin.ModelAdmin):
#     list_display = ('name','trial_period','trial_unit')
# admin.site.register(Subscription, SubscriptionAdmin)
#
#
#
# class TransactionAdmin(admin.ModelAdmin):
#     date_hierarchy = 'timestamp'
#     list_filter = ('subscription', 'user')
# admin.site.register(Transaction, TransactionAdmin)
#
# def _user(trans):
#     if trans.user != None:
#         return u'<a href="/admin/auth/user/%d/">%s</a>' % (
#             trans.user.pk, esc(trans.user))
# _user.allow_tags = True
#
# def _subscription(trans):
#     if trans.subscription != None:
#         return u'<a href="/admin/subscription/subscription/%d/">%s</a>' % (
#             trans.subscription.pk, esc(trans.subscription))
# _subscription.allow_tags = True
#
#
# class UserSubscriptionAdmin(admin.ModelAdmin):
#     list_display = (_user, _subscription ,'active')
#     list_filter = ('active', 'subscription', )
#     date_hierarchy = 'expires'
#
#
# admin.site.register(UserSubscription, UserSubscriptionAdmin)
