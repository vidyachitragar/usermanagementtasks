import os

def ensure_dir(file_path):
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
        print("Directory created in '{}' path".format(file_path))
    else:
        print("path exists")

def create_output_dict(data_value, message):
    output_dict = dict()
    output_dict['data'] = data_value
    output_dict['message'] = str(message)

    return output_dict


def create_response(data, message):

    response = {
        "data": data,
        "message": message
    }

    return response
