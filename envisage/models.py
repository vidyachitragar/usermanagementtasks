from django.db import models
import os


class File(models.Model):

    file = models.FileField(blank=False,
                            null=False,
                            upload_to=os.path.join('envisage', 'input'))

    def __str__(self):
        return self.file.name


class MultipleFiles(models.Model):
    file1 = models.FileField(blank=False, null=False, upload_to=os.path.join('envisage', 'signature', 'upload'))
    file2 = models.FileField(blank=False, null=False, upload_to=os.path.join('envisage', 'signature', 'upload'))

    def __str__(self):
        return self.file1


class FileWithOptions(models.Model):
   file = models.FileField(blank=False,
                           null=False,
                           upload_to=os.path.join('envisage', 'input'))
   options = models.CharField(max_length=50, null=True, blank=True)
   service = models.CharField(max_length=20, null=True, blank=False)

   def __str__(self):
       return self.file.name