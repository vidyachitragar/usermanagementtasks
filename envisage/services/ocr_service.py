import cv2
import numpy as np
import requests
import pytesseract

class ImageToTextProcessor(object):
    def __init__(self):
        # print("Image Processor loaded!!")
        pass
    def azure_ocr(self,img):
        try:
            img_np = None
            subscription_key = "8e6bd5565efb4766935901a8aa2eb156"
            assert subscription_key
            vision_base_url = "https://centralindia.api.cognitive.microsoft.com/vision/v2.0/"
            text_recognition_url = vision_base_url + "read/core/asyncBatchAnalyze"
            print('custom debug hit processing image',type(img))
            image_data = img
            headers = {'Ocp-Apim-Subscription-Key': subscription_key,
                       'Content-Type': 'application/octet-stream'}
            params = {'mode': 'Handwritten'}
            #data    = {'url': image_url}
            
            response = requests.post(
                text_recognition_url, headers=headers, params=params, data=image_data)
            response.raise_for_status()
            
            operation_url = response.headers["Operation-Location"]

            # The recognized text isn't immediately available, so poll to wait for completion.
            analysis = {}
            poll = True
            while poll:
                response_final = requests.get(
                    response.headers["Operation-Location"], headers=headers)
                analysis = response_final.json()
                if ("recognitionResults" in analysis):
                    poll = False
                if ("status" in analysis and analysis['status'] == 'Failed'):
                    poll = False
            polygons =[]
            if ("recognitionResults" in analysis):
                polygons = [(line["boundingBox"], line["text"]) for line in analysis["recognitionResults"][0]["lines"]]
                output = "\n".join([j['text'] for i in analysis["recognitionResults"] for j in i['lines']])
            # Display the image and overlay it with the extracted text.

            img = np.frombuffer(img, np.uint8)
            img_np = cv2.imdecode(img, cv2.IMREAD_COLOR)
            for polygon in polygons:
                vertices = np.array([[[polygon[0][i], polygon[0][i+1]]for i in range(0, len(polygon[0]), 2)]])
                cv2.polylines(img_np, vertices, True, (255, 0, 0), 4)
            # print('custom debug generating response')
            image = cv2.imencode('.png', img_np)[1].tostring()

            return [image, output]
        except Exception as e:
            print(e)
            return "401.jpg", 'Error While Processing the image!!'

    def tesseract_ocr_service(self,img_matrix):
        im = img_matrix
        if im.shape[1]<720:
            im = cv2.resize(im,(int(im.shape[1]*2),int(im.shape[0]*2)))
        extracted_text = pytesseract.image_to_string(im, lang = "eng")
        return extracted_text.strip().replace("\n\n", "\n")
