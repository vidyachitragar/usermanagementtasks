import cv2
import numpy as np

class LogoSimilarityService:

    def __init__(self, image_path_1, image_path_2):
        self.image_path_1 = image_path_1
        self.image_path_2 = image_path_2

    # 1) Check if 2 images are equals
    def check_if_equal(self):

        self.image_1 = cv2.imread(self.image_path_1)
        self.image_2 = cv2.imread(self.image_path_2)
        if self.image_1.shape == self.image_2.shape:
            print("The images have same size and channels")
            difference = cv2.subtract(self.image_1, self.image_2)
            b, g, r = cv2.split(difference)
            if cv2.countNonZero(b) == 0 and cv2.countNonZero(g) == 0 and cv2.countNonZero(r) == 0:
                return 1
            else:
                return 0
        else:
            return 0

    # 2) Check for similarities between the 2 images
    def image_similarity(self):
        sift = cv2.xfeatures2d.SIFT_create()
        # print("sift : ",sift)
        kp_1, desc_1 = sift.detectAndCompute(self.image_1, None)
        kp_2, desc_2 = sift.detectAndCompute(self.image_2, None)

        # print("kp_1 :",kp_1, "\n\ndesc_1:",desc_1)

        index_params = dict(algorithm=0, trees=5)
        search_params = dict()

        flann = cv2.FlannBasedMatcher(index_params, search_params)
        matches = flann.knnMatch(desc_1, desc_2, k=2)
        # print("type matches", type(matches))
        # print(len(matches))
        match_points = []
        ratio = 0.6
        for idx1, idx2 in matches:
            if idx1.distance < ratio * idx2.distance:
                match_points.append(idx1)
        # print(len(match_points))
        result = cv2.drawMatches(self.image_1, kp_1, self.image_2, kp_2, match_points, None)
        # cv2.imshow("result", result)
        # cv2.waitKey(0)
        return len(match_points)

    # return output_dict


# cv2.imshow("Original", original)
# cv2.imshow("Duplicate", image_to_compare)

# cv2.destroyAllWindows()
