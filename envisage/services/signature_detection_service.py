# Import packages
import os
import cv2
import numpy as np
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf
import sys

# Import utilites
from envisage.utils.object_detection import label_map_utils
from envisage.utils.object_detection import visualization_utils
from envisage.apps import EnvisageConfig

class SignatureDetectionService:

    def __init__(self, PATH_TO_MODEL, PATH_TO_LABELS, PATH_TO_IMAGE, num_classes):
        self.PATH_TO_MODEL = PATH_TO_MODEL
        self.PATH_TO_LABELS = PATH_TO_LABELS
        self.PATH_TO_IMAGE = PATH_TO_IMAGE
        self.num_classes = num_classes
        self.image_name = PATH_TO_IMAGE.split('/')[-1].split('.')[0]

    def _load_utils(self):
        label_map = label_map_utils.load_labelmap(self.PATH_TO_LABELS)
        categories = label_map_utils.convert_label_map_to_categories(label_map, max_num_classes=self.num_classes,
                                                                    use_display_name=True)
        category_index = label_map_utils.create_category_index(categories)

        return category_index

    def _load_inference_graph(self):
        """
        Load the Tensorflow model into memory.
        """
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(self.PATH_TO_MODEL, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

            self.sess = tf.Session(graph=detection_graph)

        # Define input and output tensors (i.e. data) for the object detection classifier

        # Input tensor is the image
        image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

        # Output tensors are the detection boxes, scores, and classes
        # Each box represents a part of the image where a particular object was detected
        detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

        # Each score represents level of confidence for each of the objects.
        # The score is shown on the result image, together with the class label.
        detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
        detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

        # Number of objects detected
        num_detections = detection_graph.get_tensor_by_name('num_detections:0')

        return image_tensor, detection_boxes, detection_scores, detection_classes, num_detections

    def detect_signature(self):

        image = cv2.imread(self.PATH_TO_IMAGE)
        image_expanded = np.expand_dims(image, axis=0)
        image_tensor, detection_boxes, detection_scores, detection_classes, num_detections = self._load_inference_graph()

        # Perform the actual detection by running the model with the image as input
        (boxes, scores, classes, num) = self.sess.run(
            [detection_boxes, detection_scores, detection_classes, num_detections],
            feed_dict={image_tensor: image_expanded})
        category_index = self._load_utils()

        # Draw the results of the detection (aka 'visualize the results')
        image, cropped_path_list, returned_count = visualization_utils.visualize_boxes_and_labels_on_image_array(
            image,
            np.squeeze(boxes),
            np.squeeze(classes).astype(np.int32),
            np.squeeze(scores),
            category_index,
            use_normalized_coordinates=True,
            line_thickness=8,
            min_score_thresh=0.90,
            reference_name=self.image_name,
            modeling_type='signature')

        # All the results have been drawn on image. Now display the image.
        self.sess.close()
        cv2.imwrite(os.path.join(EnvisageConfig.signature_output_path, self.image_name + ".jpg"), image)

        output_dict = dict()
        output_dict['num_signatures'] = returned_count
        output_dict['signatures'] = cropped_path_list

        return output_dict
        # cv2.imshow('Object detector', image)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()


if __name__ == "__main__":

    # This is needed since the notebook is stored in the object_detection folder.
    sys.path.append("..")
    os.getcwd()

    # Name of the directory containing the object detection module we're using
    model_name = 'inference_graph'
    image_name = 'jpg001.jpg'
    CWD_PATH = os.getcwd()
    # ml_platform
    OD_PATH = os.path.join(CWD_PATH, 'media')

    PATH_TO_MODEL = os.path.join(OD_PATH, 'resources', 'signature_resources', 'frozen_inference_graph.pb')

    PATH_TO_LABELS = os.path.join(OD_PATH, 'resources', 'signature_resources', 'signature_label_map.pbtxt')

    PATH_TO_IMAGE = os.path.join(CWD_PATH, image_name)

    num_classes = 1
    
    sign_detect_obj = SignatureDetectionService(PATH_TO_MODEL, PATH_TO_LABELS, PATH_TO_IMAGE, num_classes)
    sign_detect_obj.detect_signature()

