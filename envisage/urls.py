from django.conf.urls import url
from .views import SignatureDetectionView, LogoDetectionView, LogoSimilarityView,OcrServiceView

urlpatterns = [
    url('signature/', SignatureDetectionView.as_view(), name="file-upload"),
    url('logo/', LogoDetectionView.as_view(), name="file-upload"),
    url('logo_similarity/', LogoSimilarityView.as_view()),
    url('ocr/', OcrServiceView.as_view()),
]