from rest_framework import serializers
from .models import File
from .models import MultipleFiles, FileWithOptions


class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = "__all__"


class MultiFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = MultipleFiles
        fields = "__all__"


class OcrSerializer(serializers.ModelSerializer):
   class Meta:
       model = FileWithOptions
       fields = "__all__"