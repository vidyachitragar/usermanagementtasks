from django.apps import AppConfig
import os
from ml_platform.settings import BASE_DIR, MEDIA_ROOT

class EnvisageConfig(AppConfig):
    name = 'envisage'
    version = 'v1'

    # Signature detection
    signature_model_path = os.path.join(BASE_DIR, name, 'resources', 'signature_detection', version, 'signature_frozen_inference_graph.pb')
    signature_label_path = os.path.join(BASE_DIR, name, 'resources', 'signature_detection', version, 'signature_label_map.pbtxt')

    # Logo detection
    logo_model_path = os.path.join(BASE_DIR, name, 'resources', 'logo_detection', version, 'logo_frozen_inference_graph.pb')
    logo_label_path = os.path.join(BASE_DIR, name, 'resources', 'logo_detection', version, 'logo_label_map.pbtxt')

    # File write paths - Signature detection
    signature_input_path = os.path.join(MEDIA_ROOT, 'envisage', 'signature', 'input')
    signature_output_path = os.path.join(MEDIA_ROOT, 'envisage', 'signature', 'output')
    signature_cropped_path = os.path.join(MEDIA_ROOT, 'envisage', 'signature', 'cropped')

    # File write paths - Logo detection
    logo_input_path = os.path.join(MEDIA_ROOT, 'envisage', 'logo', 'input')

    logo_output_path = os.path.join(MEDIA_ROOT, 'envisage', 'logo', 'output')
    logo_cropped_path = os.path.join(MEDIA_ROOT, 'envisage', 'logo', 'cropped')