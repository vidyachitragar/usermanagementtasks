from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import FileUploadParser
from rest_framework import status
from .serializers import FileSerializer, MultiFileSerializer, OcrSerializer
from envisage.apps import EnvisageConfig
from envisage.services.signature_detection_service import SignatureDetectionService
from envisage.services.logo_detection_service import LogoDetectionService
from envisage.services.logo_similarity_service import LogoSimilarityService
import cv2
import io
import os
import numpy as np
from PIL import Image
from envisage.services.ocr_service import ImageToTextProcessor
from envisage.utils.commons.validator import Validation
from envisage.utils.commons.imageprocessing import Image_Handler
from envisage.utils.commons.filters import Filters

#AnalyticsDataBase
from analytics.models import AnalyticsDataBase
from datetime import datetime

#Subscription Service
from subscription.models import SubscribedUser ,Subscription ,SubscriptionType ,SubscriptionService ,Service

#Auth
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.models import User


# LOADS
data_logging = True
fil = Filters()
handler = Image_Handler()
validator = Validation()
imagetotext = ImageToTextProcessor()
Options = ["Threshold", "Adaptive Threshold", "Optimize Image", "Default Enhance Text", "Default Enhance Object",
           'Apply Sharpness', 'Adjust Exposure']


class SignatureDetectionView(APIView):

    #Auth
    permission_classes = [IsAuthenticated]

    parser_class = (FileUploadParser,)
    def post(self, request):

        validity = list((Subscription.objects.get(user_id=User.objects.get(username=request.user).pk).subscription_type).filter().values('duration'))

            ###############################Subscription Check(s)################################
        if (Service.objects.get(user_id=User.objects.get(username=request.user).pk).name) == self.__class__.__name__:
            if validity[0]['duration']  > 0:
                name = (Subscription.objects.get(user_id=(User.objects.get(username=request.user).pk)).subscription_type).filter().values('name')

                ###############################Logic############################################
                file_serializer = FileSerializer(data=request.data)

                if file_serializer.is_valid():
                    file_serializer.save()

                    file_path = file_serializer.data.get('file')
                    print("\n\nfile path in signature view is", file_path)
                    x = cv2.imread(file_path)
                    print("type of x", type(x))
                    print("shape of x", x.shape)
                    signature_detection_obj = SignatureDetectionService(EnvisageConfig.signature_model_path,
                                                                        EnvisageConfig.signature_label_path,
                                                                        file_path,
                                                                        1)
                    output_dict = signature_detection_obj.detect_signature()





                    ##################################################################
                    #For AnalyticsDataBase
                    user_id = User.objects.get(username=request.user).pk

                    data = AnalyticsDataBase(
                                             service_name=self.__class__.__name__,
                                             app_name="envisage",
                                             time=datetime.now(),
                                             user_id=user_id
                                             )

                    data.save()
                    ##################################################################


                    return Response(output_dict, status=status.HTTP_201_CREATED)
                else:
                    return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

                    ###############################Logic End ############################################
            else:
                return Response({"Message":"Validity has expired ,kindly renew"})

            #################################################################################







class LogoDetectionView(APIView):

    parser_class = (FileUploadParser,)
    permission_classes = [IsAuthenticated]


    def post(self, request):

        file_serializer = FileSerializer(data=request.data)
        if file_serializer.is_valid():
            file_serializer.save()
            file_path = file_serializer.data.get('file')

            logo_detection_obj = LogoDetectionService(EnvisageConfig.logo_model_path,
                                                      EnvisageConfig.logo_label_path,
                                                      file_path,
                                                      1)
            output_dict = logo_detection_obj.detect_logo()

            ##################################################################
            #For AnalyticsDataBase
            user_id = User.objects.get(username=request.user).pk

            data = AnalyticsDataBase(
                                     service_name=self.__class__.__name__,
                                     app_name="envisage",
                                     time=datetime.now(),
                                     user_id=user_id
                                     )

            data.save()
            ##################################################################


            return Response(output_dict, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LogoSimilarityView(APIView):

    parser_class = (FileUploadParser,)
    permission_classes = [IsAuthenticated]



    def post(self, request):
        ##################################################################
        #For AnalyticsDataBase
        user_id = User.objects.get(username=request.user).pk

        data = AnalyticsDataBase(
                                 service_name=self.__class__.__name__,
                                 app_name="envisage",
                                 time=datetime.now(),
                                 user_id=user_id
                                 )

        data.save()
        ##################################################################

        file_serializer = MultiFileSerializer(data=request.data)
        if file_serializer.is_valid():
            file_serializer.save()
            file1 = file_serializer.data.get('file1')
            file2 = file_serializer.data.get('file2')
            # print("\n\nfile_path variable in models is", file_path)
            logo_similarity_obj = LogoSimilarityService(file1, file2)
            equality = logo_similarity_obj.check_if_equal()
            similarity = logo_similarity_obj.image_similarity()

            if similarity == 1:
                similarity = "yes"
            else:
                similarity = "no"
            output_dict = dict()
            output_dict['is_equal'] = equality
            output_dict['is_similar'] = similarity
            output_dict['percentage_similarity'] = ''


            return Response(output_dict, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class options(APIView):
    """docstring for options"""

    def get(self, request):
        return Response({'options': Options})


class OcrServiceView(APIView):

    permission_classes = [IsAuthenticated]


    def post(self, request):
        ##################################################################
        #For AnalyticsDataBase
        user_id = User.objects.get(username=request.user).pk

        data = AnalyticsDataBase(
                                 service_name=self.__class__.__name__,
                                 app_name="envisage",
                                 time=datetime.now(),
                                 user_id=user_id
                                 )

        data.save()
        ##################################################################
        # No data Logging when flag OFF

        output_dict = dict()
        # output_dict['meta'] = {"feature": "NA",
        #                        "version": EnvisageConfig.version}
        if data_logging == False:
            print('step1')
            try:
                service_type = request.POST['service']
                option = request.POST['options']
                if "0" == str(service_type):
                    output_dict = dict()
                    # output_dict['meta'] = {"feature": "cloud","version": EnvisageConfig.version}
                    # output_dict['File'] = None
                    print('step2')
                    if 'file' in request.FILES:  # get file from request
                        print('step3')
                        if validator.validate_format(request.FILES['file'].name):
                            print('step4')
                            try:
                                filename = request.FILES['file'].read()
                                if option != None:
                                    nparr = np.frombuffer(filename, np.uint8)
                                    img_np = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
                                    img, _ = handler.preprocessing_options(option=option, fil=fil, image_np=img_np)
                                    data = imagetotext.azure_ocr(img)
                                else:
                                    data = imagetotext.azure_ocr(img)
                                output_dict['data'] = {"text": data[1]}
                                return Response(output_dict, status=status.HTTP_201_CREATED)
                            except Exception as e:
                                output_dict['data'] = {"text": 'Error processing file on azure services'}
                                return Response(str(e), status=status.HTTP_503_SERVICE_UNAVAILABLE)
                        else:
                            output_dict['data'] = {"text": 'file format not supported'}
                            return Response(output_dict, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        output_dict['data'] = {"text": "File missing in request"}
                        return Response(output_dict, status=status.HTTP_206_PARTIAL_CONTENT)
                        # return HttpResponse("File missing in request")


                elif "1" == str(service_type):
                    output_dict = dict()
                    # output_dict['meta'] = {"feature": "local",
                    #                            "version": EnvisageConfig.version}
                    if 'file' in request.FILES:  # get file from request
                        if validator.validate_format(request.FILES['file'].name):
                            try:
                                filename = request.FILES['file'].read()
                                nparr = np.frombuffer(filename, np.uint8)
                                img_np = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
                                if len(img_np.shape) < 3:
                                    im = Image.open(io.BytesIO(filename))
                                    img_np = np.asarray(im)[:, :, ::-1]
                                    if np.all(img_np == 0):
                                        output_dict['data'] = {"text": "Something went wrong check your data"}
                                        return Response(output_dict, status=status.HTTP_400_BAD_REQUEST)
                                _, img = handler.preprocessing_options(option=option, fil=fil, image_np=img_np)
                                data = imagetotext.tesseract_ocr_service(img)
                                output_dict['data'] = {"text": data}
                                return Response(output_dict, status=status.HTTP_201_CREATED)
                            except Exception as e:
                                output_dict['data'] = {"text": "Tesseract Extractor failed to process image!!"}
                                return Response(output_dict, status=status.HTTP_503_SERVICE_UNAVAILABLE)
                        else:
                            output_dict['data'] = {"text": "Invalid image file format!"}
                            return Response(output_dict, status=status.HTTP_400_BAD_REQUEST)
                else:
                    # output_dict['meta'] = {"feature": "NA",
                    #        "version": EnvisageConfig.version}
                    output_dict['data'] = {"text": "Service code not available in request"}
                    return Response(output_dict, status=status.HTTP_400_BAD_REQUEST)
            except Exception as e:
                print(e)
                output_dict['data'] = {"text": "Malformed request data request"}
                # output_dict['meta'] = {"feature": "NA",
                #            "version": EnvisageConfig.version}


                return Response(output_dict, status=HTTP_206_PARTIAL_CONTENT)



        # Active data Logging when flag ON

        elif data_logging == True:
            file = None
            ocr_file_serializer = OcrSerializer(data=request.data)
            if ocr_file_serializer.is_valid():
                ocr_file_serializer.save()
                file = ocr_file_serializer.data.get('file')
                option = ocr_file_serializer.data.get('options')
                service = ocr_file_serializer.data.get('service')
                print(option, type(file), option != None)
            else:
                print("not valid")
                output_dict = dict()
                # output_dict['meta'] = {"feature": "NA",
                #            "version": EnvisageConfig.version}
                # output_dict['File'] = None
                output_dict['data'] = {"text": 'Malformed request data!!'}
                return Response(output_dict, status=status.HTTP_206_PARTIAL_CONTENT)
            head, filename = os.path.split(file)
            if service == '0':
                output_dict = dict()
                # output_dict['meta'] = {"feature": "cloud",
                #            "version": EnvisageConfig.version}
                # output_dict['File'] = None
                if validator.validate_format(filename):
                    try:
                        cnt, img_np = handler.file_parser(filename, file)
                    except:
                        output_dict['data'] = {"text": "Failed to parse the file"}

                    print(option)
                    if option != None:
                        try:
                            img, _ = handler.preprocessing_options(option=option, fil=fil, image_np=img_np[0])
                        except:
                            output_dict['data'] = {"text": "ERR cannot execute filters"}
                    try:
                        data = imagetotext.azure_ocr(img)
                        output_dict['data'] = {"text": data[1]}
                    except:
                        output_dict['data'] = {"text": "Cognitive service request failed!!"}
                else:
                    output_dict['data'] = {"text": "File format not supported"}


            elif service == '1':
                output_dict = dict()
                # output_dict['meta'] = {"feature": "local",
                #            "version": EnvisageConfig.version}
                # output_dict['File'] = None
                if validator.validate_format(filename):
                    try:
                        cnt, img_np = handler.file_parser(filename, file)
                    except:
                        output_dict['data'] = {"text": "Error while parsing the file!!"}
                    if option != None:
                        try:
                            _, img = handler.preprocessing_options(option=option, fil=fil, image_np=img_np[0])
                        except:
                            output_dict['data'] = {"text": "Image preprocessor failed to process image!!"}
                            return Response(output_dict, status=status.HTTP_503_SERVICE_UNAVAILABLE)
                    try:
                        data = imagetotext.tesseract_ocr_service(img)
                        output_dict['data'] = {"text": data}
                    except:
                        output_dict['data'] = {"text": "Local OCR service failed to process image!!"}
                        return Response(output_dict, status=status.HTTP_503_SERVICE_UNAVAILABLE)

                else:
                    output_dict['data'] = {"text": "File format Not supported!!"}
                    return Response(output_dict, status=status.HTTP_201_CREATED)
            else:
                # output_dict = dict()
                # output_dict['meta'] = {"feature": "NA",
                #            "version": EnvisageConfig.version}
                output_dict['data'] = {"text": "No Service preference found in request!!"}
                return Response(output_dict, status=status.HTTP_206_PARTIAL_CONTENT)
        return Response(output_dict)
